#!/usr/bin/env python
# -*- coding:utf8 -*-
#
# DSA Spiel Analysator
# A simple GUI implementation to visualize what to expect from rolling dice
# in the pen-and-paper roleplay game 'Das Schwarze Auge' (The Dark Eye).
#
# Version:  0.3
# Usage:    python DSA.py
#
# Copyright (c) 2016 Jan D. Behrens, <zykure42@gmail.com>
#
# The application logo is based on this public domain drawing:
# https://commons.wikimedia.org/wiki/File:Twenty_sided_dice.svg
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import wx
from xml.etree import ElementTree

import dsagui as gui
import dsaitems as items
import dsaicon as icon

########################################################################

class MyCheckPanel(gui.CheckPanel):

    def __init__( self, parent ):
        gui.CheckPanel.__init__(self, parent)

        self.buttonAdd.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )
        self.buttonRemove.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-remove", wx.ART_MENU) )

        self.window = self.GetTopLevelParent()
        self.checkdict = dict(items.CHECKS)

        self.comboBoxName.Clear()
        for name, check in items.CHECKS:
            self.comboBoxName.Append(name)
        self.comboBoxName.SetSelection(0)

        self.spinCtrlModifier.SetValue(0)
        self.setProbability(0)

    def remove( self, event ):
        self.window.removePanelItem( self.window.panelChecks, self )

    def addBelow( self, event ):
        sizer = self.window.panelChecks.GetSizer()

        before = self.window.findSizerItem( sizer, self )
        self.window.createCheck( before=self )

    def updateChecks( self, event ):
        selected = self.comboBoxName.GetStringSelection()

        # try to update required checks from a predefined skill
        if selected in self.checkdict:
            checks = self.checkdict[selected]
            self.choiceCheck.SetSelection(self.choiceCheck.FindString(checks.upper()))

        self.update( event )

    def update( self, event ):
        # get current values
        checks   = [ self.choiceCheck.GetStringSelection() ]
        value    = self.window.getProperty(checks[0])
        modifier = self.spinCtrlModifier.GetValue()

        self.setValues( value )

        # roll, roll, roll the dice...
        meanSuccess, meanCrits = self.window.baseRoll( value, modifier )

        # update results
        self.setProbability( meanSuccess )

    def setProbability( self, value ):
        self.gaugeProbability.SetValue( 100 * value)
        self.staticTextProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        self.staticTextProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setValues( self, value ):
        self.staticTextValue.SetLabel( u"{} ±".format(value) )

########################################################################

class MySkillPanel(gui.SkillPanel):

    def __init__( self, parent ):
        gui.SkillPanel.__init__(self, parent)

        self.buttonAdd.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )
        self.buttonRemove.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-remove", wx.ART_MENU) )

        self.window = self.GetTopLevelParent()
        self.skilldict = dict(items.SKILLS)

        self.comboBoxName.Clear()
        for name, check in items.SKILLS:
            self.comboBoxName.Append(name)
        self.comboBoxName.SetSelection(0)

        self.spinCtrlValue.SetValue(0)
        self.spinCtrlModifier.SetValue(0)
        self.setProbability(0)
        self.setQuality(0)

    def remove( self, event ):
        self.window.removePanelItem( self.window.panelSkills, self )

    def addBelow( self, event ):
        sizer = self.window.panelSkills.GetSizer()

        before = self.window.findSizerItem( sizer, self )
        self.window.createSkill( before=self )

    def updateChecks( self, event ):
        selected = self.comboBoxName.GetStringSelection()

        # try to update required checks from a predefined skill
        if selected in self.skilldict:
            checks = self.skilldict[selected].split('/')
            self.choiceCheck1.SetSelection(self.choiceCheck1.FindString(checks[0].upper()))
            self.choiceCheck2.SetSelection(self.choiceCheck2.FindString(checks[1].upper()))
            self.choiceCheck3.SetSelection(self.choiceCheck3.FindString(checks[2].upper()))

        self.update( event )

    def update( self, event ):
        # get current values
        checks   = [ self.choiceCheck1.GetStringSelection(),
                     self.choiceCheck2.GetStringSelection(),
                     self.choiceCheck3.GetStringSelection() ]
        value    = self.spinCtrlValue.GetValue()
        modifier = self.spinCtrlModifier.GetValue()

        # roll, roll, roll the dice...
        meanSuccess, meanCrits, meanQuality = self.window.skillRoll( checks, value, modifier )

        # update results
        self.setProbability( meanSuccess )
        self.setQuality( meanQuality )

    def setProbability( self, value ):
        self.gaugeProbability.SetValue( 100 * value)
        self.staticTextProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        self.staticTextProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setQuality( self, value ):
        self.gaugeQuality.SetValue( 10 * value )  # range 0 .. 60
        self.staticTextQuality.SetLabel( "QS: {}".format(round(value, 1)) )
        self.staticTextQuality.SetForegroundColour( self.window.getQualityColour(value) )

########################################################################

class MySpellPanel(gui.SpellPanel):

    def __init__( self, parent ):
        gui.SpellPanel.__init__(self, parent)

        self.buttonAdd.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )
        self.buttonRemove.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-remove", wx.ART_MENU) )

        self.window = self.GetTopLevelParent()
        self.skilldict = dict(items.SPELLS)

        self.comboBoxName.Clear()
        for name, check in items.SPELLS:
            self.comboBoxName.Append(name)
        self.comboBoxName.SetSelection(0)

        self.spinCtrlValue.SetValue(0)
        self.spinCtrlModifier.SetValue(0)
        self.setProbability(0)
        self.setQuality(0)

    def remove( self, event ):
        self.window.removePanelItem( self.window.panelSpells, self )

    def addBelow( self, event ):
        sizer = self.window.panelSpells.GetSizer()

        before = self.window.findSizerItem( sizer, self )
        self.window.createSpell( before=self )

    def updateChecks( self, event ):
        selected = self.comboBoxName.GetStringSelection()

        # try to update required checks from a predefined skill
        if selected in self.skilldict:
            checks = self.skilldict[selected].split('/')
            self.choiceCheck1.SetSelection(self.choiceCheck1.FindString(checks[0].upper()))
            self.choiceCheck2.SetSelection(self.choiceCheck2.FindString(checks[1].upper()))
            self.choiceCheck3.SetSelection(self.choiceCheck3.FindString(checks[2].upper()))

        self.update( event )

    def update( self, event ):
        # get current values
        checks   = [ self.choiceCheck1.GetStringSelection(),
                     self.choiceCheck2.GetStringSelection(),
                     self.choiceCheck3.GetStringSelection() ]
        value    = self.spinCtrlValue.GetValue()
        modifier = self.spinCtrlModifier.GetValue()

        # roll, roll, roll the dice...
        meanSuccess, meanCrits, meanQuality = self.window.skillRoll( checks, value, modifier )

        # update results
        self.setProbability( meanSuccess )
        self.setQuality( meanQuality )

    def setProbability( self, value ):
        self.gaugeProbability.SetValue( 100 * value)
        self.staticTextProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        self.staticTextProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setQuality( self, value ):
        self.gaugeQuality.SetValue( 10 * value )  # range 0 .. 60
        self.staticTextQuality.SetLabel( "QS: {}".format(round(value, 1)) )
        self.staticTextQuality.SetForegroundColour( self.window.getQualityColour(value) )

########################################################################

class MyMeleeWeaponPanel(gui.MeleeWeaponPanel):

    def __init__( self, parent ):
        gui.MeleeWeaponPanel.__init__(self, parent)

        self.buttonAdd.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )
        self.buttonRemove.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-remove", wx.ART_MENU) )

        self.window = self.GetTopLevelParent()
        self.weapondict = dict(items.MELEE_WEAPONS)

        self.comboBoxName.Clear()
        for name, data in items.MELEE_WEAPONS:
            self.comboBoxName.Append(name)
        self.comboBoxName.SetSelection(0)

        self.spinCtrlValue.SetValue(6)
        self.spinCtrlAT.SetValue(0)
        self.spinCtrlPA.SetValue(0)
        self.setAttackProbability(0)
        self.setDefenseProbability(0)

    def remove( self, event ):
        self.window.removePanelItem( self.window.panelMeleeWeapons, self )

    def addBelow( self, event ):
        sizer = self.window.panelMeleeWeapons.GetSizer()

        before = self.window.findSizerItem( sizer, self )
        self.window.createMeleeWeapon( before=self )

    def updateChecks( self, event ):
        selected = self.comboBoxName.GetStringSelection()

        # try to update required checks from a predefined skill
        if selected in self.weapondict:
            checks, attack, defense = self.weapondict[selected]
            self.choiceCheck.SetSelection( self.choiceCheck.FindString(checks.upper()) )
            self.spinCtrlAT.SetValue(attack)
            self.spinCtrlPA.SetValue( defense if defense != None else 0 )

            self.spinCtrlPA.Enable( defense != None )
            self.gaugeDefenseProbability.Enable( defense != None )

        self.update( event )

    def update( self, event ):
        # get current values
        checks  = self.choiceCheck.GetStringSelection().split('/')
        value   = self.spinCtrlValue.GetValue()
        attack  = self.spinCtrlAT.GetValue()
        defense = self.spinCtrlPA.GetValue()

        defensive = self.spinCtrlPA.IsEnabled()  # some weapons do not allow defense

        if len(checks) > 1:
            # find highest value if more than check one is allowed (e.g. GE/KK)
            check, maxprop = None, 0
            for name in checks:
                prop = self.window.getProperty(name)
                if prop > maxprop:
                    check, maxprop = name, prop
        else:
            check = checks[0]

        atvalue = value + max(0, ( self.window.getProperty('MU') - 8 ) / 3)
        pavalue = round(value / 2., 0) + max(0, ( self.window.getProperty(check) - 8 ) / 3)

        self.setValues( atvalue, pavalue if defensive else 0 )

        # roll, roll, roll the dice...
        meanAttack,  meanAttackCrits  = self.window.baseRoll( atvalue, attack )
        meanDefense, meanDefenseCrits = self.window.baseRoll( pavalue, defense ) if defensive else (None, None)

        # update results
        self.setAttackProbability( meanAttack )
        self.setDefenseProbability( meanDefense )

    def setAttackProbability( self, value ):
        self.gaugeAttackProbability.SetValue( 100 * value )
        self.staticTextAttackProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        self.staticTextAttackProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setDefenseProbability( self, value ):
        if value != None:
            self.gaugeDefenseProbability.SetValue( 100 * value )
            self.staticTextDefenseProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        else:
            self.gaugeDefenseProbability.SetValue( 0 )
            self.staticTextDefenseProbability.SetLabel( u"N/V" )
        self.staticTextDefenseProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setValues( self, atvalue, pavalue ):
        self.staticTextAT.SetLabel( u"{} ±".format(atvalue) )
        self.staticTextPA.SetLabel( u"{} ±".format(pavalue) )

########################################################################

class MyRangeWeaponPanel(gui.RangeWeaponPanel):

    def __init__( self, parent ):
        gui.RangeWeaponPanel.__init__(self, parent)

        self.buttonAdd.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )
        self.buttonRemove.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-remove", wx.ART_MENU) )

        self.window = self.GetTopLevelParent()
        self.weapondict = dict(items.RANGE_WEAPONS)

        self.comboBoxName.Clear()
        for name, data in items.RANGE_WEAPONS:
            self.comboBoxName.Append(name)
        self.comboBoxName.SetSelection(0)

        self.spinCtrlValue.SetValue(6)
        self.spinCtrlFW.SetValue(0)
        self.setAttackProbability(0)

    def remove( self, event ):
        self.window.removePanelItem( self.window.panelRangeWeapons, self )

    def addBelow( self, event ):
        sizer = self.window.panelRangeWeapons.GetSizer()

        before = self.window.findSizerItem( sizer, self )
        self.window.createRangeWeapon( before=self )

    def updateChecks( self, event ):
        selected = self.comboBoxName.GetStringSelection()

        # try to update required checks from a predefined skill
        if selected in self.weapondict:
            checks, attack, defense = self.weapondict[selected]  # checks/defense are not used
            self.spinCtrlFW.SetValue(attack)

        self.update( event )

    def update( self, event ):
        # get current values
        value   = self.spinCtrlValue.GetValue()
        attack  = self.spinCtrlFW.GetValue()

        fwvalue = value + max(0, ( self.window.getProperty('FF') - 8 ) / 3)

        self.setValues( fwvalue )

        # roll, roll, roll the dice...
        meanAttack, meanAttackCrits = self.window.baseRoll( fwvalue, attack )

        # update results
        self.setAttackProbability( meanAttack )

    def setAttackProbability( self, value ):
        self.gaugeProbability.SetValue( 100 * value )
        self.staticTextProbability.SetLabel( "{}%".format(round(100 * value, 1)) )
        self.staticTextProbability.SetForegroundColour( self.window.getProbabilityColour(value) )

    def setValues( self, fwvalue ):
        self.staticTextFW.SetLabel( u"{} ±".format(fwvalue) )

########################################################################

class MyMainFrame(gui.MainFrame):

    VERSION = 0.1

    PCOLORS = [
        ( 0.80, (   0, 191,   0 ) ),
        ( 0.67, (  63, 159,   0 ) ),
        ( 0.50, (  95, 127,   0 ) ),
        ( 0.33, ( 127, 127,   0 ) ),
        ( 0.20, ( 151,  63,   0 ) ),
        ( 0.05, ( 191,   0,   0 ) ),
        ( 0.00, ( 255,   0,   0 ) ),
    ]

    QSCOLORS = [
        ( 5,    (   0, 191,   0 ) ),
        ( 4,    (  63, 175,   0 ) ),
        ( 3,    (  79, 159,   0 ) ),
        ( 2,    (  95, 143,   0 ) ),
        ( 1.5,  ( 111, 127,   0 ) ),
        ( 1.0,  ( 127, 127,   0 ) ),
        ( 0.5,  ( 191,   0,   0 ) ),
        ( 0.0,  ( 255,   0,   0 ) ),
    ]

    def __init__( self, parent ):
        gui.MainFrame.__init__(self, parent)

        self.SetIcon( icon.logo.GetIcon() )

        #self.buttonReset.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-new", wx.ART_TOOLBAR) )
        self.buttonSave.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-save-as", wx.ART_TOOLBAR) )
        self.buttonOpen.SetBitmapLabel( wx.ArtProvider.GetBitmap("gtk-open", wx.ART_TOOLBAR) )

        self.buttonFillChecks.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-refresh", wx.ART_MENU) )
        self.buttonClearChecks.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-clear", wx.ART_MENU) )
        self.buttonAddCheck.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )

        self.buttonFillSkills.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-refresh", wx.ART_MENU) )
        self.buttonClearSkills.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-clear", wx.ART_MENU) )
        self.buttonAddSkill.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )

        self.buttonFillSpells.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-refresh", wx.ART_MENU) )
        self.buttonClearSpells.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-clear", wx.ART_MENU) )
        self.buttonAddSpell.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )

        self.buttonFillMeleeWeapons.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-refresh", wx.ART_MENU) )
        self.buttonClearMeleeWeapons.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-clear", wx.ART_MENU) )
        self.buttonAddMeleeWeapon.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )

        self.buttonFillRangeWeapons.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-refresh", wx.ART_MENU) )
        self.buttonClearRangeWeapons.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-clear", wx.ART_MENU) )
        self.buttonAddRangeWeapon.SetBitmapLabel(  wx.ArtProvider.GetBitmap("gtk-add", wx.ART_MENU) )

        self.propCtrls = {
            'MU':       self.spinCtrlMU,
            'KL':       self.spinCtrlKL,
            'IN':       self.spinCtrlIN,
            'CH':       self.spinCtrlCH,
            'FF':       self.spinCtrlFF,
            'GE':       self.spinCtrlGE,
            'KO':       self.spinCtrlKO,
            'KK':       self.spinCtrlKK,
            'GW/LeP':   self.spinCtrlGWLeP,
            'GW/SK':    self.spinCtrlGWSK,
            'GW/ZK':    self.spinCtrlGWZK,
            'GW/GS':    self.spinCtrlGWGS,
            'LE':       self.choiceLE,
            'SK':       self.spinCtrlSK,
            'ZK':       self.spinCtrlZK,
        }

        self.skillCache = {}
        self.baseCache  = {}

        self.reset( None )
        self.fillChecks( None )
        self.update( None )

    @classmethod
    def getProbabilityColour( self, value ):
        if value != None:
            for thresh, color in self.PCOLORS:
                if round(value,2) >= thresh:
                    return color

        return ( 0, 0, 0 )

    @classmethod
    def getQualityColour( self, value ):
        if value != None:
            for thresh, color in self.QSCOLORS:
                if round(value,1) >= thresh:
                    return color

        return ( 0, 0, 0 )

    @classmethod
    def clearPanelItems( self, panel ):
        sizer = panel.GetSizer()
        sizer.DeleteWindows()
        sizer.Layout()
        panel.FitInside()

    @classmethod
    def removePanelItem( self, panel, item ):
        sizer = panel.GetSizer()
        pos = self.findSizerItem( sizer, item )
        if pos >= 0:
            item.Hide()
            sizer.Remove( pos )
            sizer.Layout()
            panel.FitInside()

    @classmethod
    def addPanelItem( self, panel, item, before=None ):
        sizer = panel.GetSizer()
        pos = -1
        if before != None:
            pos = self.findNextSizerItem( sizer, before )
        if pos >= 0:
            sizer.Insert( pos, item, 0, wx.ALL | wx.EXPAND, 0 )
        else:
            sizer.Add( item, 0, wx.ALL | wx.EXPAND, 0 )
        sizer.Layout()
        panel.FitInside()

    @classmethod
    def findSizerItem( self, sizer, item ):
        pos = -1
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            pos += 1
            if widget == item:
                break
        return pos

    @classmethod
    def findNextSizerItem( self, sizer, item ):
        pos = self.findSizerItem( sizer, item )
        if pos >= 0:
            return pos + 1
        return pos

    ### event handlers

    def resetConfig( self, event ):
        if wx.MessageBox(u"Alle Änderungen gehen verloren! Wirklich zurücksetzen?", "Zurücksetzen",
                wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
            return

        self.reset( event )
        self.fillChecks( None )
        self.update( event )

    def saveConfig( self, event ):
        chooser = wx.FileDialog( self, u"In Datei speichern", "", "", "XML files (*.xml)|*.xml",
                wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if chooser.ShowModal() == wx.ID_CANCEL:
            return

        self.saveXML( chooser.GetPath() )

    def loadConfig( self, event ):
        chooser = wx.FileDialog( self, u"Aus Datei laden", "", "", "XML files (*.xml)|*.xml",
                wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if chooser.ShowModal() == wx.ID_CANCEL:
            return

        self.loadXML( chooser.GetPath() )
        self.update( event )

    def rollDice( self, event ):
        #self.rolls = self.spinCtrlRolls.GetValue()

        self.update( event )

    def update( self, event ):
        self.message(u"Aktualisiere ...")

        self.updateBase( event )
        self.updateChecks( event )
        self.updateSkills( event )
        self.updateSpells( event )
        self.updateMeleeWeapons( event )
        self.updateRangeWeapons( event )

        self.message(u"Alle Werte aktualisiert")

    def updateBase( self, event ):
        self.message(u"Aktualisiere Basiswerte ...")

        value = round((self.getProperty('MU') + self.getProperty('GE')) / 2., 0)
        self.spinCtrlINI.SetValue( value )
        self.spinCtrlINI.SetRange( value, value )

        value = round(self.getProperty('GW/GS'), 0)
        self.spinCtrlGS.SetValue( value )
        self.spinCtrlGS.SetRange( value, value )

        value = round(self.getProperty('GW/LeP') + 2. * self.getProperty('KO'), 0)
        self.spinCtrlLeP.SetValue( value )
        self.spinCtrlLeP.SetRange( value, value )

        base = self.getProperty('LE')
        value = round(20 + self.getProperty(base), 0) if base else 0
        self.spinCtrlAsP.SetValue( value )
        self.spinCtrlAsP.SetRange( value, value )

        value = round(self.getProperty('GW/SK') +
                        (self.getProperty('MU') + self.getProperty('KL') + self.getProperty('IN')) / 6., 0)
        self.spinCtrlSK.SetValue( value )
        self.spinCtrlSK.SetRange( value, value )

        value = round(self.getProperty('GW/ZK') +
                        (self.getProperty('KO') + self.getProperty('KO') + self.getProperty('KK')) / 6., 0)
        self.spinCtrlZK.SetValue( value )
        self.spinCtrlZK.SetRange( value, value )

        value = round(self.getProperty('KO') / 2., 0)
        self.spinCtrlWS.SetValue( value )
        self.spinCtrlWS.SetRange( value, value )

        self.message(u"Alle Basiswerte aktualisiert")

    def updateChecks( self, event ):
        self.message(u"Aktualisiere Eigenschaften ...")

        sizer = self.panelChecks.GetSizer()
        for item in sizer.GetChildren():
            panel = item.GetWindow()
            if panel != None:
                panel.update( event )

    def addCheck( self, event ):
        self.createCheck()

    def fillChecks( self, event ):
        self.panelChecks.Hide()
        for name, checks in items.CHECKS:
            self.createCheck( name, checks.split('/') )
        self.panelChecks.Show()

    def clearChecks( self, event ):
        self.clearPanelItems( self.panelChecks )
        self.message(u"Alle Eigenschaften entfernt")

    def updateSkills( self, event ):
        self.message(u"Aktualisiere Fertigkeiten ...")

        sizer = self.panelSkills.GetSizer()
        for item in sizer.GetChildren():
            panel = item.GetWindow()
            if panel != None:
                panel.update( event )

    def addSkill( self, event ):
        self.createSkill()

    def fillSkills( self, event ):
        self.panelSkills.Hide()
        for name, checks in items.SKILLS:
            self.createSkill( name, checks.split('/') )
        self.panelSkills.Show()

    def clearSkills( self, event ):
        self.clearPanelItems( self.panelSkills )
        self.message(u"Alle Fertigkeiten entfernt")

    def updateSpells( self, event ):
        self.message(u"Aktualisiere Zauber ...")

        sizer = self.panelSpells.GetSizer()
        for item in sizer.GetChildren():
            panel = item.GetWindow()
            if panel != None:
                panel.update( event )

    def addSpell( self, event ):
        self.createSpell()

    def fillSpells( self, event ):
        self.panelSpells.Hide()
        for name, checks in items.SPELLS:
            self.createSpell( name, checks.split('/') )
        self.panelSpells.Show()

    def clearSpells( self, event ):
        self.clearPanelItems( self.panelSpells )
        self.message(u"Alle Zauber entfernt")

    def updateMeleeWeapons( self, event ):
        self.message(u"Aktualisiere Nahkampfwaffen ...")

        sizer = self.panelMeleeWeapons.GetSizer()
        for item in sizer.GetChildren():
            panel = item.GetWindow()
            if panel != None:
                panel.update( event )

    def addMeleeWeapon( self, event ):
        self.createMeleeWeapon()

    def fillMeleeWeapons( self, event ):
        self.panelMeleeWeapons.Hide()
        for name, values in items.MELEE_WEAPONS:
            checks, attack, defense = values
            self.createMeleeWeapon( name, checks.split('/'), 6, attack, defense )
        self.panelMeleeWeapons.Show()

    def clearMeleeWeapons( self, event ):
        self.clearPanelItems( self.panelMeleeWeapons )
        self.message(u"Alle Nahkampfwaffen entfernt")

    def updateRangeWeapons( self, event ):
        self.message(u"Aktualisiere Fernkampfwaffen ...")

        sizer = self.panelRangeWeapons.GetSizer()
        for item in sizer.GetChildren():
            panel = item.GetWindow()
            if panel != None:
                panel.update( event )

    def addRangeWeapon( self, event ):
        self.createRangeWeapon()

    def fillRangeWeapons( self, event ):
        self.panelRangeWeapons.Hide()
        for name, values in items.RANGE_WEAPONS:
            checks, attack, defense = values
            self.createRangeWeapon( name, checks.split('/'), 6, attack )
        self.panelRangeWeapons.Show()

    def clearRangeWeapons( self, event ):
        self.clearPanelItems( self.panelRangeWeapons )
        self.message(u"Alle Fernkampfwaffen entfernt")

    ### methods

    def reset( self, event=None ):
        self.textCtrlName.SetValue( u"Namenloser" )
        for key in [ 'MU', 'KL', 'IN', 'CH', 'FF', 'GE', 'KO', 'KK' ]:
            self.propCtrls[key].SetValue(8)
        self.propCtrls['GW/LeP'].SetValue(5)
        self.propCtrls['GW/SK'].SetValue(-5)
        self.propCtrls['GW/ZK'].SetValue(-5)
        self.propCtrls['GW/GS'].SetValue(8)
        self.propCtrls['LE'].SetStringSelection("")

        self.clearChecks( event )
        self.clearSkills( event )
        self.clearSpells( event )
        self.clearMeleeWeapons( event )
        self.clearRangeWeapons( event )

        self.message(u"Konfiguration zurückgesetzt")

    def loadXML( self, filename='config.xml' ):
        def getvalue(elem, key, default):
            if key in elem.attrib:
                return elem.attrib[key]
            if key.lower() in elem.attrib:
                return elem.attrib[key.lower()]
            if key.upper() in elem.attrib:
                return elem.attrib[key.upper()]
            return default

        self.reset()

        self.message(u"Lese Konfiguration: %s" % (filename))
        tree = ElementTree.parse(filename)

        root = tree.getroot()
        self.textCtrlName.SetValue( getvalue(root, 'name', u"Namenloser") )

        # settings
        settings = root.findall('settings')[0]
        for key in [ 'MU', 'KL', 'IN', 'CH', 'FF', 'GE', 'KO', 'KK' ]:
            self.propCtrls[key].SetValue( int(getvalue(settings, key, 8)) )
        self.propCtrls['GW/LeP'].SetValue( int(getvalue(settings, 'LeP', 5)) )
        self.propCtrls['GW/SK'].SetValue(  int(getvalue(settings, 'SK', -5)) )
        self.propCtrls['GW/ZK'].SetValue(  int(getvalue(settings, 'ZK', -5)) )
        self.propCtrls['GW/GS'].SetValue(  int(getvalue(settings, 'GS',  8)) )
        self.propCtrls['LE'].SetStringSelection( getvalue(settings, 'LE', "") )

        # checks
        self.panelChecks.Hide()
        for properties in root.findall('properties'):
            for prop in properties.findall('property'):
                name     = getvalue(prop, 'name', u"???")
                check    = getvalue(prop, 'check', "MU")
                modifier = int(getvalue(prop, 'modifier', 0))
                self.createCheck( name, check.split('/'), modifier )
        self.panelChecks.Show()

        # skills
        self.panelSkills.Hide()
        for skills in root.findall('skills'):
            for skill in skills.findall('skill'):
                name     = getvalue(skill, 'name', u"???")
                check    = getvalue(skill, 'check', "MU/MU/MU")
                value    = int(getvalue(skill, 'value', 0))
                modifier = int(getvalue(skill, 'modifier', 0))
                self.createSkill( name, check.split('/'), value, modifier )
        self.panelSkills.Show()

        # spells
        self.panelSpells.Hide()
        for spells in root.findall('spells'):
            for spell in spells.findall('spell'):
                name     = getvalue(spell, 'name', u"???")
                check    = getvalue(spell, 'check', "MU/MU/MU")
                value    = int(getvalue(spell, 'value', 0))
                modifier = int(getvalue(spell, 'modifier', 0))
                self.createSpell( name, check.split('/'), value, modifier )
        self.panelSpells.Show()

        # weapons
        self.panelMeleeWeapons.Hide()
        self.panelRangeWeapons.Hide()
        for weapons in root.findall('weapons'):
            for meleeweapons in weapons.findall('melee'):
                for weapon in meleeweapons.findall('weapon'):
                    name     = getvalue(weapon, 'name', u"???")
                    check    = getvalue(weapon, 'check', "KK")
                    value    = int(getvalue(weapon, 'value', 0))
                    attack   = int(getvalue(weapon, 'attack', 0))
                    defense  = int(getvalue(weapon, 'defense', 0))
                    self.createMeleeWeapon( name, check.split('/'), value, attack, defense )
            for rangeweapons in weapons.findall('range'):
                for weapon in rangeweapons.findall('weapon'):
                    name     = getvalue(weapon, 'name', u"???")
                    check    = getvalue(weapon, 'check', "FF")
                    value    = int(getvalue(weapon, 'value', 0))
                    attack   = int(getvalue(weapon, 'attack', 0))
                    self.createRangeWeapon( name, check.split('/'), value, attack )
        self.panelMeleeWeapons.Show()
        self.panelRangeWeapons.Show()

    def saveXML( self, filename="config.xml" ):
        def indent(elem, level=0, spaces=2):
            # https://stackoverflow.com/questions/3095434/inserting-newlines-in-xml-file-generated-via-xml-etree-elementtree-in-python
            i = "\n" + level * spaces * " "
            if len(elem):
                if not elem.text or not elem.text.strip():
                    elem.text = i + spaces * " "
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
                for elem in elem:
                    indent(elem, level+1)
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
            else:
                if level and (not elem.tail or not elem.tail.strip()):
                    elem.tail = i

        self.message(u"Sichere Konfiguration: %s" % (filename))

        root = ElementTree.Element('dsa')
        root.attrib['version'] = str(self.VERSION)
        root.attrib['name']    = self.textCtrlName.GetValue()

        # settings
        settings = ElementTree.SubElement(root, 'settings')
        for key in [ 'MU', 'KL', 'IN', 'CH', 'FF', 'GE', 'KO', 'KK' ]:
            settings.attrib[key] = str(self.getProperty(key))
        settings.attrib['LeP'] = str(self.getProperty('GW/LeP'))
        settings.attrib['SK']  = str(self.getProperty('GW/SK'))
        settings.attrib['ZK']  = str(self.getProperty('GW/ZK'))
        settings.attrib['GS']  = str(self.getProperty('GW/GS'))
        settings.attrib['LE']  = self.getProperty('LE')

        # checks
        props = ElementTree.SubElement(root, 'properties')
        for name, checks, modifier in self.getChecks():
            elem = ElementTree.SubElement(props, 'property',
                attrib={ 'name': name, 'check': '/'.join(checks), 'modifier': str(modifier) })

        # skills
        skills = ElementTree.SubElement(root, 'skills')
        for name, checks, value, modifier in self.getSkills():
            elem = ElementTree.SubElement(skills, 'skill',
                attrib={ 'name': name, 'check': '/'.join(checks), 'value': str(value), 'modifier': str(modifier) })

        # spells
        spells = ElementTree.SubElement(root, 'spells')
        for name, checks, value, modifier in self.getSpells():
            elem = ElementTree.SubElement(spells, 'spell',
                attrib={ 'name': name, 'check': '/'.join(checks), 'value': str(value), 'modifier': str(modifier) })

        # weapons
        weapons = ElementTree.SubElement(root, 'weapons')
        meleeweapons = ElementTree.SubElement(weapons, 'melee')
        for name, checks, value, attack, defense in self.getMeleeWeapons():
            elem = ElementTree.SubElement(meleeweapons, 'weapon',
                    attrib={ 'name': name, 'check': '/'.join(checks), 'value': str(value), 'attack': str(attack), 'defense': str(defense) })
        rangeweapons = ElementTree.SubElement(weapons, 'range')
        for name, checks, value, attack in self.getRangeWeapons():
            elem = ElementTree.SubElement(rangeweapons, 'weapon',
                    attrib={ 'name': name, 'check': '/'.join(checks), 'value': str(value), 'attack': str(attack) })

        indent(root)
        etree = ElementTree.ElementTree(root)
        etree.write(filename, encoding='utf-8')

    def message( self, text="" ):
        self.statusBar.SetStatusText( text, 0 )

    def createCheck( self, name="", checks=[], modifier=0, before=None ):
        panel = MyCheckPanel( self.panelChecks )
        if name:
            panel.comboBoxName.SetValue( name )
        if checks:
            assert( len(checks) == 1 )
            panel.choiceCheck.SetSelection( panel.choiceCheck.FindString(checks[0].upper()) )
        if modifier != None:
            panel.spinCtrlModifier.SetValue( modifier )
        panel.update( None )

        self.addPanelItem( self.panelChecks, panel, before )

        self.message(u"Eigenschaft hinzugefügt: %s [%s]" % (name, '/'.join(checks)))

    def getChecks( self ):
        props = []

        sizer = self.panelChecks.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MyCheckPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck.GetStringSelection() ]
                modifier= widget.spinCtrlModifier.GetValue()
                props.append( (name, checks, modifier) )

        return props

    def getChecks( self ):
        props = []

        sizer = self.panelChecks.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MyCheckPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck.GetStringSelection() ]
                modifier= widget.spinCtrlModifier.GetValue()
                props.append( (name, checks, modifier) )

        return props

    def createSkill( self, name="", checks=[], value=0, modifier=0, before=None ):
        panel = MySkillPanel( self.panelSkills )
        if name:
            panel.comboBoxName.SetValue( name )
        if checks:
            assert( len(checks) == 3 )
            panel.choiceCheck1.SetSelection( panel.choiceCheck1.FindString(checks[0].upper()) )
            panel.choiceCheck2.SetSelection( panel.choiceCheck2.FindString(checks[1].upper()) )
            panel.choiceCheck3.SetSelection( panel.choiceCheck3.FindString(checks[2].upper()) )
        if value != None:
            panel.spinCtrlValue.SetValue( value )
        if modifier != None:
            panel.spinCtrlModifier.SetValue( modifier )
        panel.update( None )

        self.addPanelItem( self.panelSkills, panel, before )

        self.message(u"Fertigkeit hinzugefügt: %s [%s]" % (name, '/'.join(checks)))

    def getSkills( self ):
        skills = []

        sizer = self.panelSkills.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MySkillPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck1.GetStringSelection(),
                            widget.choiceCheck2.GetStringSelection(),
                            widget.choiceCheck3.GetStringSelection() ]
                value   = widget.spinCtrlValue.GetValue()
                modifier= widget.spinCtrlModifier.GetValue()
                skills.append( (name, checks, value, modifier) )

        return skills

    def getSkills( self ):
        skills = []

        sizer = self.panelSkills.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MySkillPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck1.GetStringSelection(),
                            widget.choiceCheck2.GetStringSelection(),
                            widget.choiceCheck3.GetStringSelection() ]
                value   = widget.spinCtrlValue.GetValue()
                modifier= widget.spinCtrlModifier.GetValue()
                skills.append( (name, checks, value, modifier) )

        return skills

    def createSpell( self, name="", checks=[], value=0, modifier=0, before=None ):
        panel = MySpellPanel( self.panelSpells )
        if name:
            panel.comboBoxName.SetValue( name )
        if checks:
            assert( len(checks) == 3 )
            panel.choiceCheck1.SetSelection( panel.choiceCheck1.FindString(checks[0].upper()) )
            panel.choiceCheck2.SetSelection( panel.choiceCheck2.FindString(checks[1].upper()) )
            panel.choiceCheck3.SetSelection( panel.choiceCheck3.FindString(checks[2].upper()) )
        if value != None:
            panel.spinCtrlValue.SetValue( value )
        if modifier != None:
            panel.spinCtrlModifier.SetValue( modifier )
        panel.update( None )

        self.addPanelItem( self.panelSpells, panel, before )

        self.message(u"Zauber hinzugefügt: %s [%s]" % (name, '/'.join(checks)))

    def getSpells( self ):
        spells = []

        sizer = self.panelSpells.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MySpellPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck1.GetStringSelection(),
                            widget.choiceCheck2.GetStringSelection(),
                            widget.choiceCheck3.GetStringSelection() ]
                value   = widget.spinCtrlValue.GetValue()
                modifier= widget.spinCtrlModifier.GetValue()
                spells.append( (name, checks, value, modifier) )

        return spells

    def getSpells( self ):
        spells = []

        sizer = self.panelSpells.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MySpellPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck1.GetStringSelection(),
                            widget.choiceCheck2.GetStringSelection(),
                            widget.choiceCheck3.GetStringSelection() ]
                value   = widget.spinCtrlValue.GetValue()
                modifier= widget.spinCtrlModifier.GetValue()
                spells.append( (name, checks, value, modifier) )

        return spells

    def createMeleeWeapon( self, name="", checks=[], value=0, attack=0, defense=0, before=None ):
        panel = MyMeleeWeaponPanel( self.panelMeleeWeapons )
        if name:
            panel.comboBoxName.SetValue( name )
        if checks:
            assert( len(checks) >= 1 )
            panel.choiceCheck.SetSelection( panel.choiceCheck.FindString('/'.join(checks).upper()) )
        if value != None:
            panel.spinCtrlValue.SetValue( value )
        if attack != None:
            panel.spinCtrlAT.SetValue( attack )
        if defense != None:
            panel.spinCtrlPA.SetValue( defense )
        else:
            panel.spinCtrlPA.SetValue( 0 )
            panel.spinCtrlPA.Disable()
        panel.update( None )

        self.addPanelItem( self.panelMeleeWeapons, panel, before )

        self.message(u"Nahkampfwaffe hinzugefügt: %s [%s]" % (name, '/'.join(checks)))

    def getMeleeWeapons( self ):
        weapons = []

        sizer = self.panelMeleeWeapons.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MyMeleeWeaponPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = [ widget.choiceCheck.GetStringSelection() ]
                value   = widget.spinCtrlValue.GetValue()
                attack  = widget.spinCtrlAT.GetValue()
                defense = widget.spinCtrlPA.GetValue()
                weapons.append( (name, checks, value, attack, defense) )

        return weapons

    def getMeleeWeapons( self ):
        weapons = []

        sizer = self.panelRangeWeapons.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MyMeleeWeaponPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = widget.choiceCheck.GetStringSelection().split('/')
                value   = widget.spinCtrlValue.GetValue()
                attack  = widget.spinCtrlAT.GetValue()
                defense = widget.spinCtrlPA.GetValue()
                weapons.append( (name, checks, value, attack, defense) )

        return weapons

    def createRangeWeapon( self, name="", checks=[], value=0, attack=0, before=None ):
        sizer = self.panelRangeWeapons.GetSizer()

        panel = MyRangeWeaponPanel( self.panelRangeWeapons )
        if name:
            panel.comboBoxName.SetValue( name )
        if checks:
            assert( len(checks) >= 1 )
            panel.choiceCheck.SetSelection( panel.choiceCheck.FindString('/'.join(checks).upper()) )
        if value != None:
            panel.spinCtrlValue.SetValue( value )
        if attack != None:
            panel.spinCtrlFW.SetValue( attack )
        panel.update( None )

        self.addPanelItem( self.panelRangeWeapons, panel, before )

        self.message(u"Fernkampfwaffe hinzugefügt: %s" % (name))

    def getRangeWeapons( self ):
        weapons = []

        sizer = self.panelRangeWeapons.GetSizer()
        for child in sizer.GetChildren():
            widget = child.GetWindow()
            if isinstance(widget, MyRangeWeaponPanel):
                name    = widget.comboBoxName.GetValue()
                checks  = widget.choiceCheck.GetStringSelection().split('/')
                value   = widget.spinCtrlValue.GetValue()
                attack  = widget.spinCtrlFW.GetValue()
                weapons.append( (name, value, attack) )

        return weapons

    def getProperty( self, key ):
        if key in self.propCtrls:
            if key in [ 'LE' ]:
                return self.propCtrls[key].GetStringSelection()
            else:
                return self.propCtrls[key].GetValue()
        elif key == 'AW':
            return round(self.getProperty('GE') / 2., 0)

        return None

    def skillRoll( self, checks, value=0, modifier=0 ):
        assert( len(checks) == 3 )

        if value == None:
            return 0.

        props = [ self.getProperty(name) + modifier for name in checks ]

        key = ("%d/%d/%d" % tuple(props)) + (":%+d" % (value))
        if not key in self.skillCache:
            wins, crits, quali = 0, 0, 0
            for i in range(1,21):
                for j in range(1,21):
                    for k in range(1,21):

                        if i == 1 and j == 1 and k == 1:
                            # epic success
                            wins  += 1
                            crits += 1
                            quali += 6
                            continue
                        if i == 20 and j == 20 and k == 20:
                            # epic fail
                            crits += 1
                            quali += 6
                            continue
                        if (i == 1 and j == 1) or (j == 1 and k == 1) or (i == 1 and k == 1):
                            # critical success
                            wins  += 1
                            crits += 1
                            #quali += 0
                            continue
                        if (i == 20 and j == 20) or (j == 20 and k == 20) or (i == 20 and k == 20):
                            # critical fail
                            crits += 1
                            #quali += 0
                            continue

                        diff = value - max(0, i - props[0]) - max(0, j - props[1]) - max(0, k - props[2])
                        if diff < 0:
                            # normal fail
                            continue

                        wins  += 1
                        quali += min(6, max(1, (diff - 1) / 3 + 1))  # limited to 1..6 on success

            meanSuccess = wins  / 8000.
            meanCrits   = crits / 8000.
            meanQuality = quali / 8000.

            self.skillCache[key] = ( meanSuccess, meanCrits, meanQuality )

        return self.skillCache[key]

    def baseRoll( self, value=0, modifier=0 ):

        if value == None:
            return 0.

        value += modifier

        key = ("%+d" % value)
        if not key in self.baseCache:
            wins, crits = 0, 0
            for i in range(1,21):
                if i == 1:
                    # critical success
                    wins  += 1
                    crits += 1
                    continue
                if i == 20:
                    # critical fail
                    crits += 1
                    continue

                if i > value:
                    # normal fail
                    continue

                wins += 1

            meanSuccess = wins  / 20.
            meanCrits   = crits / 20.

            self.baseCache[key] = ( meanSuccess, meanCrits )

        return self.baseCache[key]

########################################################################

app = wx.App()
window = MyMainFrame(None)
window.Show(True)
app.MainLoop()

########################################################################
