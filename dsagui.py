# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Sep  8 2010)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"DSA Spiel Analysator", pos = wx.DefaultPosition, size = wx.Size( 1000,800 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 800,600 ), wx.DefaultSize )
		
		self.statusBar = self.CreateStatusBar( 3, wx.ST_SIZEGRIP, wx.ID_ANY )
		bSizer = wx.BoxSizer( wx.VERTICAL )
		
		self.panel0 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer0 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer110 = wx.BoxSizer( wx.VERTICAL )
		
		self.buttonOpen = wx.BitmapButton( self.panel0, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonOpen.SetToolTipString( u"Konfiguration aus Datei laden." )
		
		self.buttonOpen.SetToolTipString( u"Konfiguration aus Datei laden." )
		
		bSizer110.Add( self.buttonOpen, 0, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 2 )
		
		self.buttonSave = wx.BitmapButton( self.panel0, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonSave.SetToolTipString( u"Konfiguration in Datei speichern." )
		
		self.buttonSave.SetToolTipString( u"Konfiguration in Datei speichern." )
		
		bSizer110.Add( self.buttonSave, 0, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 2 )
		
		bSizer0.Add( bSizer110, 0, wx.ALIGN_CENTER, 5 )
		
		bSizer111 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText111a = wx.StaticText( self.panel0, wx.ID_ANY, u"MU", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText111a.Wrap( -1 )
		self.staticText111a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText111a.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		
		bSizer111.Add( self.staticText111a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlMU = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlMU.SetToolTipString( u"Haupteigenschaft: Mut (MU)\n\nBeschreibt Furchtlosigkeit, Entschlossenheit und Willensstärke." )
		
		bSizer111.Add( self.spinCtrlMU, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText111b = wx.StaticText( self.panel0, wx.ID_ANY, u"Mut", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText111b.Wrap( -1 )
		bSizer111.Add( self.staticText111b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer111, 1, wx.EXPAND, 5 )
		
		bSizer112 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText112a = wx.StaticText( self.panel0, wx.ID_ANY, u"KL", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText112a.Wrap( -1 )
		self.staticText112a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText112a.SetForegroundColour( wx.Colour( 136, 0, 204 ) )
		
		bSizer112.Add( self.staticText112a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlKL = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlKL.SetToolTipString( u"Haupteigenschaft: Klugheit (KL)\n\nBeschreibt Analysefähigkeit, Allgemeinwissen und Erinnerungsverögen." )
		
		bSizer112.Add( self.spinCtrlKL, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText112b = wx.StaticText( self.panel0, wx.ID_ANY, u"Klugheit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText112b.Wrap( -1 )
		bSizer112.Add( self.staticText112b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer112, 1, wx.EXPAND, 5 )
		
		bSizer113 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText113a = wx.StaticText( self.panel0, wx.ID_ANY, u"IN", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText113a.Wrap( -1 )
		self.staticText113a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText113a.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		
		bSizer113.Add( self.staticText113a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlIN = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlIN.SetToolTipString( u"Haupteigenschaft: Intuition (IN)\n\nBeschreibt Einfühlungsvermögen, Kreativität und Aufmerksamkeit." )
		
		bSizer113.Add( self.spinCtrlIN, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText113b = wx.StaticText( self.panel0, wx.ID_ANY, u"Intuition", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText113b.Wrap( -1 )
		bSizer113.Add( self.staticText113b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer113, 1, wx.EXPAND, 5 )
		
		bSizer114 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText114a = wx.StaticText( self.panel0, wx.ID_ANY, u"CH", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText114a.Wrap( -1 )
		self.staticText114a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText114a.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		
		bSizer114.Add( self.staticText114a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlCH = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 7 )
		self.spinCtrlCH.SetToolTipString( u"Haupteigenschaft: Charisma (CH)\n\nBeschreibt Ausstrahlung, Überzeugungskraft und Attraktivität." )
		
		bSizer114.Add( self.spinCtrlCH, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText114b = wx.StaticText( self.panel0, wx.ID_ANY, u"Charisma", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText114b.Wrap( -1 )
		bSizer114.Add( self.staticText114b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer114, 1, wx.EXPAND, 5 )
		
		bSizer115 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText115a = wx.StaticText( self.panel0, wx.ID_ANY, u"FF", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText115a.Wrap( -1 )
		self.staticText115a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText115a.SetForegroundColour( wx.Colour( 204, 204, 0 ) )
		
		bSizer115.Add( self.staticText115a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlFF = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlFF.SetToolTipString( u"Haupteigenschaft: Fingerfertigkeit (FF)\n\nBeschreibt motorische Geschicklichkeit, Koordinationsstärke und handwerkliche Fähigkeiten." )
		
		bSizer115.Add( self.spinCtrlFF, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText115b = wx.StaticText( self.panel0, wx.ID_ANY, u"Fingerfertigkeit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText115b.Wrap( -1 )
		bSizer115.Add( self.staticText115b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer115, 1, wx.EXPAND, 5 )
		
		bSizer116 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText116a = wx.StaticText( self.panel0, wx.ID_ANY, u"GE", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText116a.Wrap( -1 )
		self.staticText116a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText116a.SetForegroundColour( wx.Colour( 0, 0, 204 ) )
		
		bSizer116.Add( self.staticText116a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGE = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlGE.SetToolTipString( u"Haupteigenschaft: Gewandheit (GE)\n\nBeschreibt körperliche Geschicklichkeit, Reaktionsvermögen und Beweglichkeit." )
		
		bSizer116.Add( self.spinCtrlGE, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText116b = wx.StaticText( self.panel0, wx.ID_ANY, u"Gewandheit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText116b.Wrap( -1 )
		bSizer116.Add( self.staticText116b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer116, 1, wx.EXPAND, 5 )
		
		bSizer117 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText117a = wx.StaticText( self.panel0, wx.ID_ANY, u"KO", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText117a.Wrap( -1 )
		self.staticText117a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText117a.SetForegroundColour( wx.Colour( 153, 153, 153 ) )
		
		bSizer117.Add( self.staticText117a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlKO = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlKO.SetToolTipString( u"Haupteigenschaft: Konstitution (KO)\n\nBeschreibt Ausdauervermögen, Widerstandsfähigkeit und Belastungsstärke." )
		
		bSizer117.Add( self.spinCtrlKO, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText117b = wx.StaticText( self.panel0, wx.ID_ANY, u"Konstitution", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText117b.Wrap( -1 )
		bSizer117.Add( self.staticText117b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer117, 1, wx.EXPAND, 5 )
		
		bSizer118 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText118a = wx.StaticText( self.panel0, wx.ID_ANY, u"KK", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText118a.Wrap( -1 )
		self.staticText118a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.staticText118a.SetForegroundColour( wx.Colour( 204, 102, 0 ) )
		
		bSizer118.Add( self.staticText118a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlKK = wx.SpinCtrl( self.panel0, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 20, 8 )
		self.spinCtrlKK.SetToolTipString( u"Haupteigenschaft: Körperkraft (KK)\n\nBeschreibt körperliche Stärke, Leistungsvermögen und motorische Kontrolle." )
		
		bSizer118.Add( self.spinCtrlKK, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText118b = wx.StaticText( self.panel0, wx.ID_ANY, u"Körperkraft", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText118b.Wrap( -1 )
		bSizer118.Add( self.staticText118b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer0.Add( bSizer118, 1, wx.EXPAND, 5 )
		
		self.panel0.SetSizer( bSizer0 )
		self.panel0.Layout()
		bSizer0.Fit( self.panel0 )
		bSizer.Add( self.panel0, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.notebook = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.panel1 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.textCtrlName = wx.TextCtrl( self.panel1, wx.ID_ANY, u"Namenloser", wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE )
		self.textCtrlName.SetMaxLength( 80 ) 
		self.textCtrlName.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 93, 92, False, wx.EmptyString ) )
		self.textCtrlName.SetToolTipString( u"Name des Charakters." )
		
		bSizer1.Add( self.textCtrlName, 0, wx.ALL|wx.EXPAND, 5 )
		
		gSizer11 = wx.GridSizer( 1, 5, 0, 0 )
		
		bSizer1111 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText111a1 = wx.StaticText( self.panel1, wx.ID_ANY, u"GW: LeP", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText111a1.Wrap( -1 )
		self.staticText111a1.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1111.Add( self.staticText111a1, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGWLeP = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"5", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 0, 10, 0 )
		self.spinCtrlGWLeP.SetToolTipString( u"Grundwert: Lebensenergie (GW: LeP)\n\nBasiswert zur Berechnung der Lebensenergie (LeP) zusammen mit Konstitution (KO)." )
		
		bSizer1111.Add( self.spinCtrlGWLeP, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText111b1 = wx.StaticText( self.panel1, wx.ID_ANY, u"Grundwert Lebenspunkte", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText111b1.Wrap( 1 )
		bSizer1111.Add( self.staticText111b1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer11.Add( bSizer1111, 1, wx.EXPAND, 5 )
		
		bSizer1121 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText112a1 = wx.StaticText( self.panel1, wx.ID_ANY, u"GW: SK", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText112a1.Wrap( -1 )
		self.staticText112a1.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1121.Add( self.staticText112a1, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGWSK = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"-5", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, -10, 0, 0 )
		self.spinCtrlGWSK.SetToolTipString( u"Grundwert: Seelenkraft (GW: SK)\n\nBasiswert zur Berechnung der Seelenkraft (SK) zusammen mit Mut (MU), Klugheit (KL) und Intuition (IN)." )
		
		bSizer1121.Add( self.spinCtrlGWSK, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText112b1 = wx.StaticText( self.panel1, wx.ID_ANY, u"Grundwert Seelenkraft", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText112b1.Wrap( 1 )
		bSizer1121.Add( self.staticText112b1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer11.Add( bSizer1121, 1, wx.EXPAND, 5 )
		
		bSizer1131 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText113a1 = wx.StaticText( self.panel1, wx.ID_ANY, u"GW: ZK", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText113a1.Wrap( -1 )
		self.staticText113a1.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1131.Add( self.staticText113a1, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGWZK = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"-5", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, -10, 0, 0 )
		self.spinCtrlGWZK.SetToolTipString( u"Grundwert: Zähigkeit (GW: ZK)\n\nBasiswert zur Berechnung der Zähigkeit (ZK) zusammen mit Konstituion (KO) und Körperkraft (KK)." )
		
		bSizer1131.Add( self.spinCtrlGWZK, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText113b1 = wx.StaticText( self.panel1, wx.ID_ANY, u"Grundwert Zähigkeit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText113b1.Wrap( 1 )
		bSizer1131.Add( self.staticText113b1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer11.Add( bSizer1131, 1, wx.EXPAND, 5 )
		
		bSizer1141 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText114a1 = wx.StaticText( self.panel1, wx.ID_ANY, u"GW: GS", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText114a1.Wrap( -1 )
		self.staticText114a1.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1141.Add( self.staticText114a1, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGWGS = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 60,-1 ), wx.SP_ARROW_KEYS, 1, 12, 5 )
		self.spinCtrlGWGS.SetToolTipString( u"Grundwert: Geschwindigkeit (GW: GS)\n\nBasiswert zur Berechnung der Geschwindigkeit (GS)." )
		
		bSizer1141.Add( self.spinCtrlGWGS, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText114b1 = wx.StaticText( self.panel1, wx.ID_ANY, u"Grundwert Geschwindigkeit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText114b1.Wrap( 1 )
		bSizer1141.Add( self.staticText114b1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer11.Add( bSizer1141, 1, wx.EXPAND, 5 )
		
		bSizer1151 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText115a1 = wx.StaticText( self.panel1, wx.ID_ANY, u"LE", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText115a1.Wrap( -1 )
		self.staticText115a1.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1151.Add( self.staticText115a1, 0, wx.ALIGN_CENTER, 5 )
		
		choiceLEChoices = [ wx.EmptyString, u"KL", u"IN", u"CH" ]
		self.choiceLE = wx.Choice( self.panel1, wx.ID_ANY, wx.DefaultPosition, wx.Size( 70,-1 ), choiceLEChoices, 0 )
		self.choiceLE.SetSelection( 0 )
		self.choiceLE.SetToolTipString( u"Leiteigenschaft für Zauberei (LE).\n\nBasiswert zur Berechnung der Astral-/Karmalenergie (AsP/KaP) zusammen mit dem Grundwert für Zauberer." )
		
		bSizer1151.Add( self.choiceLE, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText115b1 = wx.StaticText( self.panel1, wx.ID_ANY, u"Leiteigenschaft Zauberei", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText115b1.Wrap( 1 )
		bSizer1151.Add( self.staticText115b1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer11.Add( bSizer1151, 1, wx.EXPAND, 5 )
		
		bSizer1.Add( gSizer11, 0, wx.EXPAND, 5 )
		
		gSizer12 = wx.GridSizer( 1, 7, 0, 0 )
		
		bSizer121 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText121a = wx.StaticText( self.panel1, wx.ID_ANY, u"INI", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText121a.Wrap( -1 )
		self.staticText121a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer121.Add( self.staticText121a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlINI = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlINI.SetToolTipString( u"Erweiterte Eigenschaft: Initiative (INI)\n\nBerechnet aus Mut (MU) und Gewandheit (GE):\nINI = MU + GE + GE." )
		
		bSizer121.Add( self.spinCtrlINI, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText121b = wx.StaticText( self.panel1, wx.ID_ANY, u"Initiative", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText121b.Wrap( -1 )
		bSizer121.Add( self.staticText121b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer121, 1, wx.EXPAND, 5 )
		
		bSizer122 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText122a = wx.StaticText( self.panel1, wx.ID_ANY, u"LeP", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText122a.Wrap( -1 )
		self.staticText122a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer122.Add( self.staticText122a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlLeP = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlLeP.SetToolTipString( u"Erweiterte Eigenschaft: Lebensenergie (LeP)\n\nBerechnet aus Grundwert (GW: LeP) und Konstitution (KO):\nLeP = GW:LeP + KO + KO." )
		
		bSizer122.Add( self.spinCtrlLeP, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText122b = wx.StaticText( self.panel1, wx.ID_ANY, u"Lebensenergie", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText122b.Wrap( -1 )
		bSizer122.Add( self.staticText122b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer122, 1, wx.EXPAND, 5 )
		
		bSizer123 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText123a = wx.StaticText( self.panel1, wx.ID_ANY, u"AsP/KaP", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText123a.Wrap( -1 )
		self.staticText123a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer123.Add( self.staticText123a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlAsP = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlAsP.SetToolTipString( u"Erweiterte Eigenschaft: Astral-/Karmalenergie (AsP/KaP)\n\nBerechnet aus Leitwert (Wert der Leiteigenschaft, LE) und Grundwerte für Zauberer:\nAsP/KaP = LE + 20." )
		
		bSizer123.Add( self.spinCtrlAsP, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText123b = wx.StaticText( self.panel1, wx.ID_ANY, u"Astralenergie/ Kamalenergie", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText123b.Wrap( 1 )
		bSizer123.Add( self.staticText123b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer123, 1, wx.EXPAND, 5 )
		
		bSizer124 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText124a = wx.StaticText( self.panel1, wx.ID_ANY, u"GS", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText124a.Wrap( -1 )
		self.staticText124a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer124.Add( self.staticText124a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlGS = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlGS.SetToolTipString( u"Erweiterte Eigenschaft: Geschwindigkeit (GS)\n\nEntspricht dem Grundwert der Geschwindigkeit (GW: GS)." )
		
		bSizer124.Add( self.spinCtrlGS, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText124b = wx.StaticText( self.panel1, wx.ID_ANY, u"Geschwindigkeit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText124b.Wrap( -1 )
		bSizer124.Add( self.staticText124b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer124, 1, wx.EXPAND, 5 )
		
		bSizer126 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText126a = wx.StaticText( self.panel1, wx.ID_ANY, u"SK", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText126a.Wrap( -1 )
		self.staticText126a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer126.Add( self.staticText126a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlSK = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlSK.SetToolTipString( u"Erweiterte Eigenschaft: Seelenkraft (SK)\n\nBerechnet aus Grundwert (GW:SK), Mut (MU), Klugheit (KL) und Intuition (IN):\nSK = GW:SK + (MU + KL + IN) / 6." )
		
		bSizer126.Add( self.spinCtrlSK, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText1246b = wx.StaticText( self.panel1, wx.ID_ANY, u"Seelenkraft", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText1246b.Wrap( -1 )
		bSizer126.Add( self.staticText1246b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer126, 1, wx.EXPAND, 5 )
		
		bSizer127 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText127a = wx.StaticText( self.panel1, wx.ID_ANY, u"ZK", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText127a.Wrap( -1 )
		self.staticText127a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer127.Add( self.staticText127a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlZK = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlZK.SetToolTipString( u"Erweiterte Eigenschaft: Zähigkeit (ZK)\n\nBerechnet aus Grundwert (GW:ZK), Konstitution (KO) und Körperkraft (KK):\nZK = GW:ZK + (KO + KO + KK) / 6." )
		
		bSizer127.Add( self.spinCtrlZK, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText127b = wx.StaticText( self.panel1, wx.ID_ANY, u"Zähigkeit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText127b.Wrap( -1 )
		bSizer127.Add( self.staticText127b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer127, 1, wx.EXPAND, 5 )
		
		bSizer125 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText125a = wx.StaticText( self.panel1, wx.ID_ANY, u"WS", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText125a.Wrap( -1 )
		self.staticText125a.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer125.Add( self.staticText125a, 0, wx.ALIGN_CENTER, 5 )
		
		self.spinCtrlWS = wx.SpinCtrl( self.panel1, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 60,-1 ), 0, 0, 0, 0 )
		self.spinCtrlWS.SetToolTipString( u"Erweiterte Eigenschaft: Wundschwelle (WS)\n\nBerechnet aus Konstitution (KO): WS = KO / 2." )
		
		bSizer125.Add( self.spinCtrlWS, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText125b = wx.StaticText( self.panel1, wx.ID_ANY, u"Wundschwelle", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticText125b.Wrap( -1 )
		bSizer125.Add( self.staticText125b, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		gSizer12.Add( bSizer125, 1, wx.EXPAND, 5 )
		
		bSizer1.Add( gSizer12, 0, wx.EXPAND, 5 )
		
		self.panelChecks = wx.ScrolledWindow( self.panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL )
		self.panelChecks.SetScrollRate( 5, 5 )
		bSizer13 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelChecks.SetSizer( bSizer13 )
		self.panelChecks.Layout()
		bSizer13.Fit( self.panelChecks )
		bSizer1.Add( self.panelChecks, 1, wx.EXPAND |wx.ALL, 5 )
		
		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonClearChecks = wx.BitmapButton( self.panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonClearChecks.SetToolTipString( u"Alle Eigenschaft entfernen." )
		
		self.buttonClearChecks.SetToolTipString( u"Alle Eigenschaft entfernen." )
		
		bSizer13.Add( self.buttonClearChecks, 0, wx.ALL, 5 )
		
		
		bSizer13.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonFillChecks = wx.BitmapButton( self.panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonFillChecks.SetToolTipString( u"Alle Eigenschaften hinzufügen." )
		
		self.buttonFillChecks.SetToolTipString( u"Alle Eigenschaften hinzufügen." )
		
		bSizer13.Add( self.buttonFillChecks, 0, wx.ALL, 5 )
		
		
		bSizer13.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonAddCheck = wx.BitmapButton( self.panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonAddCheck.SetToolTipString( u"Neue Eigenschaft hinzufügen." )
		
		self.buttonAddCheck.SetToolTipString( u"Neue Eigenschaft hinzufügen." )
		
		bSizer13.Add( self.buttonAddCheck, 0, wx.ALL, 5 )
		
		bSizer1.Add( bSizer13, 0, wx.EXPAND, 5 )
		
		self.panel1.SetSizer( bSizer1 )
		self.panel1.Layout()
		bSizer1.Fit( self.panel1 )
		self.notebook.AddPage( self.panel1, u"Eigenschaften", True )
		self.panel2 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelSkills = wx.ScrolledWindow( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.panelSkills.SetScrollRate( 5, 5 )
		bSizer21 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelSkills.SetSizer( bSizer21 )
		self.panelSkills.Layout()
		bSizer21.Fit( self.panelSkills )
		bSizer2.Add( self.panelSkills, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		bSizer2.AddSpacer( ( 0, 0), 0, wx.EXPAND, 5 )
		
		bSizer23 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonClearSkills = wx.BitmapButton( self.panel2, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonClearSkills.SetToolTipString( u"Alle Fertigkeit entfernen." )
		
		self.buttonClearSkills.SetToolTipString( u"Alle Fertigkeit entfernen." )
		
		bSizer23.Add( self.buttonClearSkills, 0, wx.ALL, 5 )
		
		
		bSizer23.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonFillSkills = wx.BitmapButton( self.panel2, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonFillSkills.SetToolTipString( u"Alle Fertigkeiten hinzufügen." )
		
		self.buttonFillSkills.SetToolTipString( u"Alle Fertigkeiten hinzufügen." )
		
		bSizer23.Add( self.buttonFillSkills, 0, wx.ALL, 5 )
		
		
		bSizer23.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonAddSkill = wx.BitmapButton( self.panel2, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonAddSkill.SetToolTipString( u"Neue Fertigkeit hinzufügen." )
		
		self.buttonAddSkill.SetToolTipString( u"Neue Fertigkeit hinzufügen." )
		
		bSizer23.Add( self.buttonAddSkill, 0, wx.ALL, 5 )
		
		bSizer2.Add( bSizer23, 0, wx.EXPAND, 5 )
		
		self.panel2.SetSizer( bSizer2 )
		self.panel2.Layout()
		bSizer2.Fit( self.panel2 )
		self.notebook.AddPage( self.panel2, u"Fertigkeiten", False )
		self.panel3 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelSpells = wx.ScrolledWindow( self.panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.panelSpells.SetScrollRate( 5, 5 )
		bSizer31 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelSpells.SetSizer( bSizer31 )
		self.panelSpells.Layout()
		bSizer31.Fit( self.panelSpells )
		bSizer3.Add( self.panelSpells, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		bSizer3.AddSpacer( ( 0, 0), 0, wx.EXPAND, 5 )
		
		bSizer33 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonClearSpells = wx.BitmapButton( self.panel3, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonClearSpells.SetToolTipString( u"Alle Zauber entfernen." )
		
		self.buttonClearSpells.SetToolTipString( u"Alle Zauber entfernen." )
		
		bSizer33.Add( self.buttonClearSpells, 0, wx.ALL, 5 )
		
		
		bSizer33.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonFillSpells = wx.BitmapButton( self.panel3, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonFillSpells.SetToolTipString( u"Alle Zauber hinzufügen." )
		
		self.buttonFillSpells.SetToolTipString( u"Alle Zauber hinzufügen." )
		
		bSizer33.Add( self.buttonFillSpells, 0, wx.ALL, 5 )
		
		
		bSizer33.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonAddSpell = wx.BitmapButton( self.panel3, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonAddSpell.SetToolTipString( u"Neuen Zauber hinzufügen." )
		
		self.buttonAddSpell.SetToolTipString( u"Neuen Zauber hinzufügen." )
		
		bSizer33.Add( self.buttonAddSpell, 0, wx.ALL, 5 )
		
		bSizer3.Add( bSizer33, 0, wx.EXPAND, 5 )
		
		self.panel3.SetSizer( bSizer3 )
		self.panel3.Layout()
		bSizer3.Fit( self.panel3 )
		self.notebook.AddPage( self.panel3, u"Zauberei", False )
		self.panel4 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelMeleeWeapons = wx.ScrolledWindow( self.panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.panelMeleeWeapons.SetScrollRate( 5, 5 )
		bSizer41 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelMeleeWeapons.SetSizer( bSizer41 )
		self.panelMeleeWeapons.Layout()
		bSizer41.Fit( self.panelMeleeWeapons )
		bSizer4.Add( self.panelMeleeWeapons, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		bSizer4.AddSpacer( ( 0, 0), 0, wx.EXPAND, 5 )
		
		bSizer43 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonClearMeleeWeapons = wx.BitmapButton( self.panel4, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonClearMeleeWeapons.SetToolTipString( u"Alle Nahkampfwaffen entfernen." )
		
		self.buttonClearMeleeWeapons.SetToolTipString( u"Alle Nahkampfwaffen entfernen." )
		
		bSizer43.Add( self.buttonClearMeleeWeapons, 0, wx.ALL, 5 )
		
		
		bSizer43.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonFillMeleeWeapons = wx.BitmapButton( self.panel4, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonFillMeleeWeapons.SetToolTipString( u"Alle Nahkampfwaffen hinzufügen." )
		
		self.buttonFillMeleeWeapons.SetToolTipString( u"Alle Nahkampfwaffen hinzufügen." )
		
		bSizer43.Add( self.buttonFillMeleeWeapons, 0, wx.ALL, 5 )
		
		
		bSizer43.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonAddMeleeWeapon = wx.BitmapButton( self.panel4, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonAddMeleeWeapon.SetToolTipString( u"Neue Nahkampfwaffe hinzufügen." )
		
		self.buttonAddMeleeWeapon.SetToolTipString( u"Neue Nahkampfwaffe hinzufügen." )
		
		bSizer43.Add( self.buttonAddMeleeWeapon, 0, wx.ALL, 5 )
		
		bSizer4.Add( bSizer43, 0, wx.EXPAND, 5 )
		
		self.panel4.SetSizer( bSizer4 )
		self.panel4.Layout()
		bSizer4.Fit( self.panel4 )
		self.notebook.AddPage( self.panel4, u"Nahkampf", False )
		self.panel5 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer5 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelRangeWeapons = wx.ScrolledWindow( self.panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.panelRangeWeapons.SetScrollRate( 5, 5 )
		bSizer51 = wx.BoxSizer( wx.VERTICAL )
		
		self.panelRangeWeapons.SetSizer( bSizer51 )
		self.panelRangeWeapons.Layout()
		bSizer51.Fit( self.panelRangeWeapons )
		bSizer5.Add( self.panelRangeWeapons, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		bSizer5.AddSpacer( ( 0, 0), 0, wx.EXPAND, 5 )
		
		bSizer53 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonClearRangeWeapons = wx.BitmapButton( self.panel5, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonClearRangeWeapons.SetToolTipString( u"Alle Fernkampfwaffen entfernen." )
		
		self.buttonClearRangeWeapons.SetToolTipString( u"Alle Fernkampfwaffen entfernen." )
		
		bSizer53.Add( self.buttonClearRangeWeapons, 0, wx.ALL, 5 )
		
		
		bSizer53.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonFillRangeWeapons = wx.BitmapButton( self.panel5, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonFillRangeWeapons.SetToolTipString( u"Alle Fernkampfwaffen hinzufügen." )
		
		self.buttonFillRangeWeapons.SetToolTipString( u"Alle Fernkampfwaffen hinzufügen." )
		
		bSizer53.Add( self.buttonFillRangeWeapons, 0, wx.ALL, 5 )
		
		
		bSizer53.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.buttonAddRangeWeapon = wx.BitmapButton( self.panel5, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 34,34 ), wx.BU_AUTODRAW )
		self.buttonAddRangeWeapon.SetToolTipString( u"Neue Fernkampfwaffe hinzufügen." )
		
		self.buttonAddRangeWeapon.SetToolTipString( u"Neue Fernkampfwaffe hinzufügen." )
		
		bSizer53.Add( self.buttonAddRangeWeapon, 0, wx.ALL, 5 )
		
		bSizer5.Add( bSizer53, 0, wx.EXPAND, 5 )
		
		self.panel5.SetSizer( bSizer5 )
		self.panel5.Layout()
		bSizer5.Fit( self.panel5 )
		self.notebook.AddPage( self.panel5, u"Fernkampf", False )
		
		bSizer.Add( self.notebook, 1, wx.EXPAND |wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.buttonOpen.Bind( wx.EVT_BUTTON, self.loadConfig )
		self.buttonSave.Bind( wx.EVT_BUTTON, self.saveConfig )
		self.spinCtrlMU.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlMU.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlKL.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlKL.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlIN.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlIN.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlCH.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlCH.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlFF.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlFF.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlGE.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGE.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlKO.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlKO.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlKK.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGWLeP.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGWLeP.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlGWSK.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGWSK.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlGWZK.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGWZK.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlGWGS.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlGWGS.Bind( wx.EVT_TEXT, self.update )
		self.choiceLE.Bind( wx.EVT_CHOICE, self.update )
		self.buttonClearChecks.Bind( wx.EVT_BUTTON, self.clearChecks )
		self.buttonFillChecks.Bind( wx.EVT_BUTTON, self.fillChecks )
		self.buttonAddCheck.Bind( wx.EVT_BUTTON, self.addCheck )
		self.buttonClearSkills.Bind( wx.EVT_BUTTON, self.clearSkills )
		self.buttonFillSkills.Bind( wx.EVT_BUTTON, self.fillSkills )
		self.buttonAddSkill.Bind( wx.EVT_BUTTON, self.addSkill )
		self.buttonClearSpells.Bind( wx.EVT_BUTTON, self.clearSpells )
		self.buttonFillSpells.Bind( wx.EVT_BUTTON, self.fillSpells )
		self.buttonAddSpell.Bind( wx.EVT_BUTTON, self.addSpell )
		self.buttonClearMeleeWeapons.Bind( wx.EVT_BUTTON, self.clearMeleeWeapons )
		self.buttonFillMeleeWeapons.Bind( wx.EVT_BUTTON, self.fillMeleeWeapons )
		self.buttonAddMeleeWeapon.Bind( wx.EVT_BUTTON, self.addMeleeWeapon )
		self.buttonClearRangeWeapons.Bind( wx.EVT_BUTTON, self.clearRangeWeapons )
		self.buttonFillRangeWeapons.Bind( wx.EVT_BUTTON, self.fillRangeWeapons )
		self.buttonAddRangeWeapon.Bind( wx.EVT_BUTTON, self.addRangeWeapon )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def loadConfig( self, event ):
		event.Skip()
	
	def saveConfig( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	def clearChecks( self, event ):
		event.Skip()
	
	def fillChecks( self, event ):
		event.Skip()
	
	def addCheck( self, event ):
		event.Skip()
	
	def clearSkills( self, event ):
		event.Skip()
	
	def fillSkills( self, event ):
		event.Skip()
	
	def addSkill( self, event ):
		event.Skip()
	
	def clearSpells( self, event ):
		event.Skip()
	
	def fillSpells( self, event ):
		event.Skip()
	
	def addSpell( self, event ):
		event.Skip()
	
	def clearMeleeWeapons( self, event ):
		event.Skip()
	
	def fillMeleeWeapons( self, event ):
		event.Skip()
	
	def addMeleeWeapon( self, event ):
		event.Skip()
	
	def clearRangeWeapons( self, event ):
		event.Skip()
	
	def fillRangeWeapons( self, event ):
		event.Skip()
	
	def addRangeWeapon( self, event ):
		event.Skip()
	

###########################################################################
## Class SkillPanel
###########################################################################

class SkillPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,50 ), style = wx.TAB_TRAVERSAL )
		
		bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonRemove = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonRemove.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonRemove.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		self.buttonRemove.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonRemove.SetToolTipString( u"Diese Fertigkeit entfernen." )
		
		bSizer1.Add( self.buttonRemove, 0, 0, 0 )
		
		self.buttonAdd = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonAdd.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonAdd.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		self.buttonAdd.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonAdd.SetToolTipString( u"Weitere Fertigkeit  hinzufügen." )
		
		bSizer1.Add( self.buttonAdd, 0, 0, 0 )
		
		bSizer.Add( bSizer1, 0, wx.ALIGN_CENTER|wx.ALL, 0 )
		
		comboBoxNameChoices = []
		self.comboBoxName = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 180,-1 ), comboBoxNameChoices, 0 )
		self.comboBoxName.SetToolTipString( u"Name der Fertigkeit." )
		self.comboBoxName.SetMinSize( wx.Size( 180,-1 ) )
		
		bSizer.Add( self.comboBoxName, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck1Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck1 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck1Choices, 0 )
		self.choiceCheck1.SetSelection( 0 )
		self.choiceCheck1.SetToolTipString( u"Erste Eigenschaft der Fertigkeitsprobe." )
		
		bSizer.Add( self.choiceCheck1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck2Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck2 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck2Choices, 0 )
		self.choiceCheck2.SetSelection( 0 )
		self.choiceCheck2.SetToolTipString( u"Zweite Eigenschaft der Fertigkeitsprobe." )
		
		bSizer.Add( self.choiceCheck2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck3Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck3 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck3Choices, 0 )
		self.choiceCheck3.SetSelection( 0 )
		self.choiceCheck3.SetToolTipString( u"Dritte Eigenschaft der Fertigkeitsprobe." )
		
		bSizer.Add( self.choiceCheck3, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticLine1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer.Add( self.staticLine1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.staticText1 = wx.StaticText( self, wx.ID_ANY, u"FW:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText1.Wrap( -1 )
		bSizer.Add( self.staticText1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlValue = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlValue.SetToolTipString( u"Fertigkeitswert zum Ausgleichen von Würfelergebnissen bei der Fertigkeitsprobe." )
		
		bSizer.Add( self.spinCtrlValue, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText2 = wx.StaticText( self, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText2.Wrap( -1 )
		bSizer.Add( self.staticText2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlModifier = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlModifier.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis) zur Anpassung der Würfelergebnisse bei der Fertigkeitsprobe." )
		
		bSizer.Add( self.spinCtrlModifier, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeProbability.SetValue( 50 ) 
		self.gaugeProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der Fertigkeitsprobe." )
		self.gaugeProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextProbability.Wrap( -1 )
		bSizer.Add( self.staticTextProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeQuality = wx.Gauge( self, wx.ID_ANY, 60, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeQuality.SetValue( 10 ) 
		self.gaugeQuality.SetToolTipString( u"Erwartete durchnschnittliche Qualitätsstufe bei der Fertigkeitsprobe." )
		self.gaugeQuality.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeQuality, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextQuality = wx.StaticText( self, wx.ID_ANY, u"QS: 7", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		self.staticTextQuality.Wrap( -1 )
		bSizer.Add( self.staticTextQuality, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		# Connect Events
		self.buttonRemove.Bind( wx.EVT_BUTTON, self.remove )
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.addBelow )
		self.comboBoxName.Bind( wx.EVT_COMBOBOX, self.updateChecks )
		self.choiceCheck1.Bind( wx.EVT_CHOICE, self.update )
		self.choiceCheck2.Bind( wx.EVT_CHOICE, self.update )
		self.choiceCheck3.Bind( wx.EVT_CHOICE, self.update )
		self.spinCtrlValue.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlValue.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_TEXT, self.update )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def remove( self, event ):
		event.Skip()
	
	def addBelow( self, event ):
		event.Skip()
	
	def updateChecks( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	
	
	
	
	

###########################################################################
## Class CheckPanel
###########################################################################

class CheckPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,50 ), style = wx.TAB_TRAVERSAL )
		
		bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonRemove = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonRemove.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonRemove.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		self.buttonRemove.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonRemove.SetToolTipString( u"Diese Eigenschaft entfernen." )
		
		bSizer1.Add( self.buttonRemove, 0, 0, 0 )
		
		self.buttonAdd = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonAdd.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonAdd.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		self.buttonAdd.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonAdd.SetToolTipString( u"Weitere Eigenschaft hinzufügen." )
		
		bSizer1.Add( self.buttonAdd, 0, 0, 0 )
		
		bSizer.Add( bSizer1, 0, wx.ALIGN_CENTER|wx.ALL, 0 )
		
		comboBoxNameChoices = []
		self.comboBoxName = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 180,-1 ), comboBoxNameChoices, 0 )
		self.comboBoxName.SetToolTipString( u"Name der Eigenschaft." )
		self.comboBoxName.SetMinSize( wx.Size( 180,-1 ) )
		
		bSizer.Add( self.comboBoxName, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheckChoices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK", u"AW" ]
		self.choiceCheck = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheckChoices, 0 )
		self.choiceCheck.SetSelection( 0 )
		self.choiceCheck.SetToolTipString( u"Eigenschaft der direkten Eigenschaftsprobe." )
		
		bSizer.Add( self.choiceCheck, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticLine1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer.Add( self.staticLine1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.staticTextValue = wx.StaticText( self, wx.ID_ANY, u"0 ±", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.staticTextValue.Wrap( -1 )
		bSizer.Add( self.staticTextValue, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlModifier = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlModifier.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis) zur Anpassung der Würfelergebnisse bei der Eigenschaftsprobe." )
		
		bSizer.Add( self.spinCtrlModifier, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeProbability.SetValue( 50 ) 
		self.gaugeProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der direkten Eigenschaftsprobe." )
		self.gaugeProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextProbability.Wrap( -1 )
		bSizer.Add( self.staticTextProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		# Connect Events
		self.buttonRemove.Bind( wx.EVT_BUTTON, self.remove )
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.addBelow )
		self.comboBoxName.Bind( wx.EVT_COMBOBOX, self.updateChecks )
		self.choiceCheck.Bind( wx.EVT_CHOICE, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_TEXT, self.update )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def remove( self, event ):
		event.Skip()
	
	def addBelow( self, event ):
		event.Skip()
	
	def updateChecks( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	

###########################################################################
## Class SpellPanel
###########################################################################

class SpellPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,50 ), style = wx.TAB_TRAVERSAL )
		
		bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonRemove = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonRemove.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonRemove.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		self.buttonRemove.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonRemove.SetToolTipString( u"Diesen Zauber entfernen." )
		
		bSizer1.Add( self.buttonRemove, 0, 0, 0 )
		
		self.buttonAdd = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonAdd.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonAdd.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		self.buttonAdd.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonAdd.SetToolTipString( u"Weiteren Zauber hinzufügen." )
		
		bSizer1.Add( self.buttonAdd, 0, 0, 0 )
		
		bSizer.Add( bSizer1, 0, wx.ALIGN_CENTER|wx.ALL, 0 )
		
		comboBoxNameChoices = []
		self.comboBoxName = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 180,-1 ), comboBoxNameChoices, 0 )
		self.comboBoxName.SetToolTipString( u"Name des Zauberspruchs, des Rituals oder der Liturgie." )
		self.comboBoxName.SetMinSize( wx.Size( 180,-1 ) )
		
		bSizer.Add( self.comboBoxName, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck1Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck1 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck1Choices, 0 )
		self.choiceCheck1.SetSelection( 0 )
		self.choiceCheck1.SetToolTipString( u"Erste Eigenschaft der Zauberprobe." )
		
		bSizer.Add( self.choiceCheck1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck2Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck2 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck2Choices, 0 )
		self.choiceCheck2.SetSelection( 0 )
		self.choiceCheck2.SetToolTipString( u"Zweite Eigenschaft der Zauberprobe." )
		
		bSizer.Add( self.choiceCheck2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheck3Choices = [ u"MU", u"KL", u"IN", u"CH", u"FF", u"GE", u"KO", u"KK" ]
		self.choiceCheck3 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheck3Choices, 0 )
		self.choiceCheck3.SetSelection( 0 )
		self.choiceCheck3.SetToolTipString( u"Dritte Eigenschaft der Zauberprobe." )
		
		bSizer.Add( self.choiceCheck3, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticLine1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer.Add( self.staticLine1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.staticText1 = wx.StaticText( self, wx.ID_ANY, u"FW:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText1.Wrap( -1 )
		bSizer.Add( self.staticText1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlValue = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlValue.SetToolTipString( u"Fertigkeitswert zum Ausgleichen von Würfelergebnissen bei der Zauberprobe." )
		
		bSizer.Add( self.spinCtrlValue, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText2 = wx.StaticText( self, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText2.Wrap( -1 )
		bSizer.Add( self.staticText2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlModifier = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlModifier.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis) zur Anpassung der Würfelergebnisse bei der Zauberprobe." )
		
		bSizer.Add( self.spinCtrlModifier, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeProbability.SetValue( 50 ) 
		self.gaugeProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der Zauberprobe." )
		self.gaugeProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextProbability.Wrap( -1 )
		bSizer.Add( self.staticTextProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeQuality = wx.Gauge( self, wx.ID_ANY, 60, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeQuality.SetValue( 10 ) 
		self.gaugeQuality.SetToolTipString( u"Erwartete durchnschnittliche Qualitätsstufe bei der Zauberprobe." )
		self.gaugeQuality.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeQuality, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextQuality = wx.StaticText( self, wx.ID_ANY, u"QS: 7", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		self.staticTextQuality.Wrap( -1 )
		bSizer.Add( self.staticTextQuality, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		# Connect Events
		self.buttonRemove.Bind( wx.EVT_BUTTON, self.remove )
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.addBelow )
		self.comboBoxName.Bind( wx.EVT_COMBOBOX, self.updateChecks )
		self.choiceCheck1.Bind( wx.EVT_CHOICE, self.update )
		self.choiceCheck2.Bind( wx.EVT_CHOICE, self.update )
		self.choiceCheck3.Bind( wx.EVT_CHOICE, self.update )
		self.spinCtrlValue.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlValue.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlModifier.Bind( wx.EVT_TEXT, self.update )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def remove( self, event ):
		event.Skip()
	
	def addBelow( self, event ):
		event.Skip()
	
	def updateChecks( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	
	
	
	
	

###########################################################################
## Class MeleeWeaponPanel
###########################################################################

class MeleeWeaponPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,50 ), style = wx.TAB_TRAVERSAL )
		
		bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonRemove = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonRemove.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonRemove.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		self.buttonRemove.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonRemove.SetToolTipString( u"Diese Nahkampfwaffe entfernen." )
		
		bSizer1.Add( self.buttonRemove, 0, 0, 0 )
		
		self.buttonAdd = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonAdd.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonAdd.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		self.buttonAdd.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonAdd.SetToolTipString( u"Weitere Nahkampfwaffe hinzufügen." )
		
		bSizer1.Add( self.buttonAdd, 0, 0, 0 )
		
		bSizer.Add( bSizer1, 0, wx.ALIGN_CENTER|wx.ALL, 0 )
		
		comboBoxNameChoices = []
		self.comboBoxName = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 180,-1 ), comboBoxNameChoices, 0 )
		self.comboBoxName.SetToolTipString( u"Name der Nahkampfwaffe." )
		
		bSizer.Add( self.comboBoxName, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheckChoices = [ u"KK", u"FF", u"GE", u"GE/KK" ]
		self.choiceCheck = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 95,-1 ), choiceCheckChoices, 0 )
		self.choiceCheck.SetSelection( 0 )
		self.choiceCheck.SetToolTipString( u"Eigenschaft der Verteidigungsprobe." )
		
		bSizer.Add( self.choiceCheck, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText1 = wx.StaticText( self, wx.ID_ANY, u"KTW:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText1.Wrap( -1 )
		bSizer.Add( self.staticText1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlValue = wx.SpinCtrl( self, wx.ID_ANY, u"6", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlValue.SetToolTipString( u"Kampftechnikwert zum Anwenden der Angriffs- und Verteidigungsprobe." )
		
		bSizer.Add( self.spinCtrlValue, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticLine1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer.Add( self.staticLine1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.staticText2 = wx.StaticText( self, wx.ID_ANY, u"AT:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText2.Wrap( -1 )
		bSizer.Add( self.staticText2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextAT = wx.StaticText( self, wx.ID_ANY, u"0 ±", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.staticTextAT.Wrap( -1 )
		bSizer.Add( self.staticTextAT, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlAT = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlAT.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis auf Attackenwert) zur Anpassung der Würfelergebnisse bei der Angriffsprobe." )
		
		bSizer.Add( self.spinCtrlAT, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeAttackProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeAttackProbability.SetValue( 50 ) 
		self.gaugeAttackProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der Angriffsprobe." )
		self.gaugeAttackProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeAttackProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextAttackProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextAttackProbability.Wrap( -1 )
		bSizer.Add( self.staticTextAttackProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticText3 = wx.StaticText( self, wx.ID_ANY, u"PA:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText3.Wrap( -1 )
		bSizer.Add( self.staticText3, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextPA = wx.StaticText( self, wx.ID_ANY, u"0 ±", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.staticTextPA.Wrap( -1 )
		bSizer.Add( self.staticTextPA, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlPA = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlPA.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis auf Paradewert) zur Anpassung der Würfelergebnisse bei der Verteidigungsprobe." )
		
		bSizer.Add( self.spinCtrlPA, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeDefenseProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeDefenseProbability.SetValue( 50 ) 
		self.gaugeDefenseProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der Verteidungsprobe." )
		self.gaugeDefenseProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeDefenseProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextDefenseProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextDefenseProbability.Wrap( -1 )
		bSizer.Add( self.staticTextDefenseProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		# Connect Events
		self.buttonRemove.Bind( wx.EVT_BUTTON, self.remove )
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.addBelow )
		self.comboBoxName.Bind( wx.EVT_COMBOBOX, self.updateChecks )
		self.choiceCheck.Bind( wx.EVT_CHOICE, self.update )
		self.spinCtrlValue.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlValue.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlAT.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlAT.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlPA.Bind( wx.EVT_SPINCTRL, self.update )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def remove( self, event ):
		event.Skip()
	
	def addBelow( self, event ):
		event.Skip()
	
	def updateChecks( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	
	
	
	

###########################################################################
## Class RangeWeaponPanel
###########################################################################

class RangeWeaponPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,50 ), style = wx.TAB_TRAVERSAL )
		
		bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.buttonRemove = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonRemove.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonRemove.SetForegroundColour( wx.Colour( 204, 0, 0 ) )
		self.buttonRemove.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonRemove.SetToolTipString( u"Diese Fernkampfwaffe entfernen." )
		
		bSizer1.Add( self.buttonRemove, 0, 0, 0 )
		
		self.buttonAdd = wx.Button( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 34,34 ), wx.NO_BORDER )
		self.buttonAdd.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		self.buttonAdd.SetForegroundColour( wx.Colour( 0, 204, 0 ) )
		self.buttonAdd.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.buttonAdd.SetToolTipString( u"Weitere Fernkampfwaffe hinzufügen." )
		
		bSizer1.Add( self.buttonAdd, 0, 0, 0 )
		
		bSizer.Add( bSizer1, 0, wx.ALIGN_CENTER|wx.ALL, 0 )
		
		comboBoxNameChoices = []
		self.comboBoxName = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 180,-1 ), comboBoxNameChoices, 0 )
		self.comboBoxName.SetToolTipString( u"Name der Fernkampfwaffe." )
		
		bSizer.Add( self.comboBoxName, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		choiceCheckChoices = [ u"FF" ]
		self.choiceCheck = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 65,-1 ), choiceCheckChoices, 0 )
		self.choiceCheck.SetSelection( 0 )
		self.choiceCheck.SetToolTipString( u"Eigenschaft der Verteidigungsprobe." )
		
		bSizer.Add( self.choiceCheck, 0, wx.ALL, 5 )
		
		self.staticText1 = wx.StaticText( self, wx.ID_ANY, u"KTW:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText1.Wrap( -1 )
		bSizer.Add( self.staticText1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlValue = wx.SpinCtrl( self, wx.ID_ANY, u"6", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, 0, 20, 0 )
		self.spinCtrlValue.SetToolTipString( u"Kampftechnikwert zum Anwenden der Angriffsprobe." )
		
		bSizer.Add( self.spinCtrlValue, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticLine1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer.Add( self.staticLine1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.staticText2 = wx.StaticText( self, wx.ID_ANY, u"FW:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText2.Wrap( -1 )
		bSizer.Add( self.staticText2, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextFW = wx.StaticText( self, wx.ID_ANY, u"0 ±", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
		self.staticTextFW.Wrap( -1 )
		bSizer.Add( self.staticTextFW, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.spinCtrlFW = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 55,-1 ), wx.SP_ARROW_KEYS, -10, 10, 0 )
		self.spinCtrlFW.SetToolTipString( u"Modifikator (Erleichterung/Erschwernis auf Fernkampfwert) zur Anpassung der Würfelergebnisse bei der Angriffsprobe." )
		
		bSizer.Add( self.spinCtrlFW, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.gaugeProbability = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( -1,-1 ), wx.GA_HORIZONTAL )
		self.gaugeProbability.SetValue( 50 ) 
		self.gaugeProbability.SetToolTipString( u"Erwartete Erfolgswahrscheinlichkeit bei der Angriffsprobe." )
		self.gaugeProbability.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer.Add( self.gaugeProbability, 1, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.staticTextProbability = wx.StaticText( self, wx.ID_ANY, u"100,0%", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextProbability.Wrap( -1 )
		bSizer.Add( self.staticTextProbability, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.SetSizer( bSizer )
		self.Layout()
		
		# Connect Events
		self.buttonRemove.Bind( wx.EVT_BUTTON, self.remove )
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.addBelow )
		self.comboBoxName.Bind( wx.EVT_COMBOBOX, self.updateChecks )
		self.choiceCheck.Bind( wx.EVT_CHOICE, self.update )
		self.spinCtrlValue.Bind( wx.EVT_SPINCTRL, self.update )
		self.spinCtrlValue.Bind( wx.EVT_TEXT, self.update )
		self.spinCtrlFW.Bind( wx.EVT_SPINCTRL, self.update )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def remove( self, event ):
		event.Skip()
	
	def addBelow( self, event ):
		event.Skip()
	
	def updateChecks( self, event ):
		event.Skip()
	
	def update( self, event ):
		event.Skip()
	
	
	
	

