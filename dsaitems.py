# -*- coding:utf8 -*-

########################################################################

CHECKS = [
    # Geistige EIgenschaften
    ( u"Mut",               "MU" ),
    ( u"Klugheit",          "KL" ),
    ( u"Intuition",         "IN" ),
    ( u"Charisma",          "CH" ),

    # Körperliche Eigenschaften
    ( u"Fingerfertigkeit",  "FF" ),
    ( u"Gewandheit",        "GE" ),
    ( u"Konstitution",      "KO" ),
    ( u"Körperkraft",       "KK" ),

    # Sonstige Eigenschaften
    ( u"Ausweichen",        "AW" ),
]

########################################################################

SKILLS = [
    # Körpertalente
    ( u"Fliegen",                   "MU/IN/GE" ),
    ( u"Gaukeleien",                "MU/CH/FF" ),
    ( u"Klettern",                  "MU/GE/KK" ),
    ( u"Körperbeherrschung",        "GE/GE/KO" ),
    ( u"Kraftakt",                  "KO/KK/KK" ),
    ( u"Reiten",                    "CH/GE/KK" ),
    ( u"Schwimmen",                 "GE/KO/KK" ),
    ( u"Selbstbeherrschung",        "MU/MU/KO" ),
    ( u"Singen",                    "KL/CH/KO" ),
    ( u"Sinnesschärfe",             "KL/IN/IN" ),
    ( u"Tanzen",                    "KL/CH/GE" ),
    ( u"Taschendiebstahl",          "KL/CH/GE" ),
    ( u"Verbergen",                 "MU/IN/GE" ),
    ( u"Zechen",                    "KL/KO/KK" ),

    # Gesellschaftstalente
    ( u"Bekehren & Überzeugen",     "MU/KL/CH" ),
    ( u"Betören",                   "MU/CH/CH" ),
    ( u"Einschüchtern",             "MU/IN/CH" ),
    ( u"Etikette",                  "KL/IN/CH" ),
    ( u"Gassenwissen",              "KL/IN/CH" ),
    ( u"Menschenkenntnis",          "KL/IN/CH" ),
    ( u"Überreden",                 "MU/IN/CH" ),
    ( u"Verkleiden",                "IN/CH/GE" ),
    ( u"Willenskraft",              "MU/IN/CH" ),

    # Naturtalente
    ( u"Fährtensuchen",             "MU/IN/GE" ),
    ( u"Fesseln",                   "KL/FF/KK" ),
    ( u"Fischen & Angeln",          "FF/GE/KO" ),
    ( u"Orientierung",              "KL/IN/IN" ),
    ( u"Pflanzenkunde",             "KL/FF/KO" ),
    ( u"Tierkunde",                 "MU/MU/CH" ),
    ( u"Wildnisleben",              "MU/GE/KO" ),

    # Wissenstalente
    ( u"Brett- & Glücksspiel",      "KL/IN/IN" ),
    ( u"Geographie",                "KL/KL/IN" ),
    ( u"Geschichtswissen",          "KL/KL/IN" ),
    ( u"Götter & Kulte",            "KL/KL/IN" ),
    ( u"Kriegskunst",               "MU/KL/IN" ),
    ( u"Magiekunde",                "MU/IN/GE" ),
    ( u"Mechanik",                  "KL/KL/FF" ),
    ( u"Rechnen",                   "KL/KL/IN" ),
    ( u"Rechtskunde",               "KL/KL/IN" ),
    ( u"Sagen & Legenden",          "KL/KL/IN" ),
    ( u"Sphärenkunde",              "KL/KL/IN" ),
    ( u"Sternkunde",                "KL/KL/IN" ),

    # Handwerkstalente
    ( u"Alchimie",                  "MU/KL/FF" ),
    ( u"Boote & Schiffe",           "FF/GE/KK" ),
    ( u"Fahrzeuge",                 "CH/FF/KO" ),
    ( u"Handel",                    "KL/IN/CH" ),
    ( u"Heilkunde Gift",            "MU/KL/IN" ),
    ( u"Heilkunde Krankheit",       "MU/IN/KO" ),
    ( u"Heilkunde Seele",           "IN/CH/KO" ),
    ( u"Heilkunde Wunden",          "KL/FF/FF" ),
    ( u"Holzbearbeitung",           "FF/GE/KK" ),
    ( u"Lebensmittelbearbeitung",   "IN/FF/FF" ),
    ( u"Lederbearbeitung",          "FF/GE/KO" ),
    ( u"Malen & Zeichnen",          "IN/FF/FF" ),
    ( u"Metallbearbeitung",         "FF/KO/KK" ),
    ( u"Musizieren",                "CH/FF/KO" ),
    ( u"Schlösserknacken",          "IN/FF/FF" ),
    ( u"Steinbearbeitung",          "FF/FF/KK" ),
    ( u"Stoffbearbeitung",          "KL/FF/FF" ),
]

########################################################################

SPELLS = [
    # Antimagie
    ( u"Disruptivo",                "MU/KL/CH" ),

    # Einfluss
    ( u"Atemnot",                   "KL/CH/KO" ),
    ( u"Friedenslied",              "KL/IN/FF" ),
    ( u"Herr über das Tierreich",   "MU/IN/CH" ),
    ( u"Zunge betäuben",            "KL/IN/CH" ),

    # Elementar
    ( u"Ignifaxius",                "MU/KL/CH" ),
    ( u"Manifesto",                 "MU/KL/CH" ),

    # Heilung
    ( u"Armatrutz",                 "KL/IN/FF" ),
    ( u"Balsam Salabunde",          "KL/IN/FF" ),
    ( u"Zaubermelodie",             "MU/IN/CH" ),

    # Hellsicht
    ( u"Odem Arcanum",              "MU/KL/IN" ),
    ( u"Pestilenz erspüren",        "MU/KL/IN" ),

    # Objekt
    ( u"Sumus Elixiere",            "KL/IN/IN" ),

    # Sphären
    ( u"Elementarer Diener",        "MU/CH/KO" ),

    # Telekinese
    ( u"Silentium",                 "KL/FF/KK" ),

    # Verwandlung
    ( u"Fulminictus",               "KL/IN/KO" ),
    ( u"Visibili",                  "KL/IN/KO" ),
    ( u"Wasseratem",                "KL/IN/KO" ),
]

########################################################################

MELEE_WEAPONS = [
    # Nahkampfwaffen
    #( u"Dolche",                ( "GE",     0,   0   ) ),
    #( u"Fechtwaffen",           ( "GE",     0,   0   ) ),
    #( u"Hiebwaffen",            ( "KK",     0,   0   ) ),
    #( u"Kettenwaffen",          ( "KK",     0,  None ) ),
    #( u"Lanzen",                ( "KK",     0,   0   ) ),
    #( u"Raufen",                ( "GE/KK",  0,   0   ) ),
    #( u"Schwerter",             ( "GE/KK",  0,   0   ) ),
    #( u"Stangenwaffen",         ( "GE/KK",  0,   0   ) ),
    #( u"Zweihandhiebwaffen",    ( "KK",     0,   0   ) ),
    #( u"Zweihandschwerter",     ( "KK",     0,   0   ) ),
    #( u"Schilde",               ( "KK",     0,   0   ) ),

    # Dolche
    ( u"Basiliskenzunge",       ( "GE",     0,  -1   ) ),
    ( u"Dolch",                 ( "GE",     0,   0   ) ),
    ( u"Drachenzahn",           ( "GE",     0,  -1   ) ),
    ( u"Linkhand",              ( "GE",     0,   0   ) ),
    ( u"Messer",                ( "GE",     0,  -2   ) ),
    ( u"Schwerer Dolch",        ( "GE",     0,   0   ) ),
    ( u"Waqqif",                ( "GE",     0,   0   ) ),

    # Fechtwaffen
    ( u"Florett",               ( "GE",     1,   0   ) ),
    ( u"Rapier",                ( "GE",     1,   0   ) ),
    ( u"Wolfsmesser",           ( "GE",     1,   1   ) ),

    # Hiebwaffen
    ( u"Brabakbengel",          ( "KK",    -1,  -2   ) ),
    ( u"Keule",                 ( "KK",     0,  -1   ) ),
    ( u"Knüppel",               ( "KK",     0,  -2   ) ),
    ( u"Lindwurmschläger",      ( "KK",     0,  -1   ) ),
    ( u"Magierstab kurz",       ( "KK",     0,  -1   ) ),
    ( u"Magierstab mittel",     ( "KK",     0,  -1   ) ),
    ( u"Molokdeschnaja",        ( "KK",     0,  -1   ) ),
    ( u"Orknase",               ( "KK",    -1,  -2   ) ),
    ( u"Rabenschnabel",         ( "KK",     0,  -1   ) ),
    ( u"Sonnenzepter",          ( "KK",     0,  -1   ) ),
    ( u"Streitaxt",             ( "KK",     0,  -1   ) ),
    ( u"Streitkolben",          ( "KK",     0,  -1   ) ),

    # Kettenwaffen
    ( u"Morgenstern",           ( "KK",     0,  None ) ),

    # Lanzen

    # Raufen
    ( u"Schlagring",            ( "GE/KK",   0, None ) ),
    ( u"Waffenlos",             ( "GE/KK",   0, None ) ),

    # Schilde
    ( u"Holzschild",            ( "KK",     -4,   1  ) ),
    ( u"Lederschild",           ( "KK",     -4,   1  ) ),
    ( u"Thorwalerschild",       ( "KK",     -5,   2  ) ),
    ( u"Großschild",            ( "KK",     -6,   3  ) ),

    # Schwerter
    ( u"Barbarenschwert",       ( "GE/KK",  -1,  -1  ) ),
    ( u"Entermesser",           ( "GE/KK",   0,  -1  ) ),
    ( u"Haumesser",             ( "GE/KK",   0,  -1  ) ),
    ( u"Khunchomer",            ( "GE/KK",   0,   0  ) ),
    ( u"Kurzschwert",           ( "GE/KK",   0,   0  ) ),
    ( u"Langschwert",           ( "GE/KK",   0,   0  ) ),
    ( u"Robbentöter",           ( "GE/KK",   0,   0  ) ),
    ( u"Säbel",                 ( "GE/KK",   0,   0  ) ),
    ( u"Sklaventod",            ( "GE/KK",   0,   0  ) ),

    # Stangenwaffen
    ( u"Dreizack",              ( "GE/KK",   0,   0  ) ),
    ( u"Dschadra",              ( "GE/KK",   0,  -1  ) ),
    ( u"Hellebarde",            ( "GE/KK",   0,  -2  ) ),
    ( u"Holzspeer",             ( "GE/KK",   0,   0  ) ),
    ( u"Kampfstab",             ( "GE/KK",   0,   2  ) ),
    ( u"Magierstab lang",       ( "GE/KK",  -1,   2  ) ),
    ( u"Speer",                 ( "GE/KK",   0,   0  ) ),
    ( u"Zweililien",            ( "GE/KK",   0,   2  ) ),

    # Zweihandhiebwaffen
    ( u"Barbarenstreitaxt",     ( "KK",      0,  -4  ) ),
    ( u"Felsspalter",           ( "KK",      0,  -2  ) ),
    ( u"Kriegshammer",          ( "KK",      0,  -3  ) ),
    ( u"Zwergenschlägel",       ( "KK",      0,  -1  ) ),

    # Zweihandschwerter
    ( u"Anderthalbhänder",      ( "KK",      0,   0  ) ),
    ( u"Doppelkhunchomer",      ( "KK",      0,  -2  ) ),
    ( u"Großer Sklaventod",     ( "KK",      0,  -2  ) ),
    ( u"Rondrakamm",            ( "KK",      0,  -1  ) ),
    ( u"Tuzakmesser",           ( "KK",      0,   0  ) ),
    ( u"Zweihänder",            ( "KK",      0,  -3  ) ),
]

########################################################################

RANGE_WEAPONS = [
    # Fernkampfwaffen
    #( u"Armbrüste",             ( "FF",     0,  None ) ),
    #( u"Bögen",                 ( "FF",     0,  None ) ),
    #( u"Wurfwaffen",            ( "FF",     0,  None ) ),

    # Armbrüste
    ( u"Balestrina",            ( "FF",      0, None ) ),
    ( u"Eisenwalder",           ( "FF",      0, None ) ),
    ( u"Handarmbrust",          ( "FF",      0, None ) ),
    ( u"Leichte Armbrust",      ( "FF",      0, None ) ),
    ( u"Schwere Armbrust",      ( "FF",      0, None ) ),

    # Bögen
    ( u"Elfenbogen",            ( "FF",      0, None ) ),
    ( u"Kompositbogen",         ( "FF",      0, None ) ),
    ( u"Kurzbogen",             ( "FF",      0, None ) ),
    ( u"Langbogen",             ( "FF",      0, None ) ),

    # Wurfwaffen
    ( u"Schneidzahn",           ( "FF",      0, None ) ),
    ( u"Stein",                 ( "FF",      0, None ) ),
    ( u"Wurfbeil",              ( "FF",      0, None ) ),
    ( u"Wurfdolch",             ( "FF",      0, None ) ),
    ( u"Wurfkeule",             ( "FF",      0, None ) ),
    ( u"Wurfring",              ( "FF",      0, None ) ),
    ( u"Wurfscheibe",           ( "FF",      0, None ) ),
    ( u"Wurfstern",             ( "FF",      0, None ) ),
    ( u"Wurfspeer",             ( "FF",      0, None ) ),
]

########################################################################
